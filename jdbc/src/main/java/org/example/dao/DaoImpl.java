package org.example.dao;

import org.example.dto.Departamento;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoImpl implements Dao{
    private Connection connection;

    private void getConnection() throws SQLException {
        this.connection = DriverManager.getConnection(
                "jdbc:mariadb://127.0.0.1:3306/poo",
                "root",
                "mariadb");
    }
    private void cerrarConexion() throws SQLException {
        connection.close();
    }
    public void agregarDepartamento(Departamento departamento) throws SQLException {
        getConnection();
        String nuevoDepartamentoSQL = "insert into departamento(codigo_departamento, nombre , codigo_locacion)"+
                " values(?,?,?)";

        PreparedStatement preparedStatement = connection.prepareStatement(nuevoDepartamentoSQL);
        preparedStatement.setInt(1, departamento.getCodigo());
        preparedStatement.setString(2, departamento.getNombre());
        preparedStatement.setInt(3, departamento.getCodigoLocacion());

        Integer filasAfectadas = preparedStatement.executeUpdate();
        System.out.println("Filas afectadas: " + filasAfectadas);
        preparedStatement.close();
        connection.commit();
        cerrarConexion();
    }

    public List<Departamento> obtenerDepartamentos() throws SQLException {
        List<Departamento> departamentos = new ArrayList<>();
        getConnection();
        Statement sentencia = connection.createStatement();
        String departamentoSQL = " select codigo_departamento, nombre , codigo_locacion" +
                " from departamento";
        ResultSet resultado = sentencia.executeQuery(departamentoSQL);

        while (resultado.next()){
            Departamento departamento = new Departamento();
            departamento.setCodigo(resultado.getInt("codigo_departamento"));
            departamento.setNombre(resultado.getString("nombre"));
            departamento.setCodigoLocacion(resultado.getInt("codigo_locacion"));
            departamentos.add(departamento);
        }
        resultado.close();
        sentencia.close();
        cerrarConexion();
        return departamentos;
    }
}
