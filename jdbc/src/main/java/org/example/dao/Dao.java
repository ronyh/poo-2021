package org.example.dao;

import org.example.dto.Departamento;

import java.sql.SQLException;
import java.util.List;

public interface Dao {
    void agregarDepartamento(Departamento departamento) throws SQLException;
    List<Departamento> obtenerDepartamentos() throws SQLException;
}
