package org.example.dao;

public abstract class SingletonDao {
    private static Dao dao;
    public static Dao getDao(){
        if(dao == null){
            dao = new DaoImpl();
        }
        return dao;
    }
}
