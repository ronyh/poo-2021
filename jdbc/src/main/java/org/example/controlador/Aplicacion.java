package org.example.controlador;

import org.example.dto.Departamento;
import org.example.servicio.SingletonService;

public class Aplicacion {
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("org.mariadb.jdbc.Driver");
        Departamento departamento = new Departamento();
        departamento.setCodigo(5);
        departamento.setNombre("Apurimac");
        departamento.setCodigoLocacion(2);
        SingletonService.getServicio().agregarDepartamento(departamento);
        for (Departamento item: SingletonService.getServicio().obtenerDepartamentos()) {
            System.out.println(item);
        }
    }
}
