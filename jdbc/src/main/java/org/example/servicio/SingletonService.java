package org.example.servicio;

public abstract class SingletonService {
    private static Servicio servicio;
    public static Servicio getServicio(){
        if(servicio == null){
            servicio = new ServicioImpl();
        }
        return servicio;
    }
}
