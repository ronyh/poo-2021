package org.example.servicio;

import org.example.dto.Departamento;

import java.util.List;

public interface Servicio {
    void agregarDepartamento(Departamento departamento);
    List<Departamento> obtenerDepartamentos();
}
