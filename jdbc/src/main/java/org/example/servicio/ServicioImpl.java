package org.example.servicio;

import org.example.dao.SingletonDao;
import org.example.dto.Departamento;

import java.sql.SQLException;
import java.util.List;

public class ServicioImpl implements Servicio{
    public void agregarDepartamento(Departamento departamento) {
        try {
            SingletonDao.getDao().agregarDepartamento(departamento);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Departamento> obtenerDepartamentos() {
        List<Departamento> lista = null;
        try {
            lista = SingletonDao.getDao().obtenerDepartamentos();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }
}
