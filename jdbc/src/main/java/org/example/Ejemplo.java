package org.example;

import java.sql.*;

public class Ejemplo {

    public static void main(String[] args) throws SQLException {
        Connection connection = null;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mariadb://127.0.0.1:3306/poo",
                    "root",
                    "mariadb");
            String nuevoDepartamentoSQL = "insert into departamento(codigo_departamento, nombre , codigo_locacion)"+
                    " values(?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(nuevoDepartamentoSQL);
            preparedStatement.setInt(1, 3);
            preparedStatement.setString(2, "Puno");
            preparedStatement.setInt(3, 2);

            Integer filasAfectadas = preparedStatement.executeUpdate();
            System.out.println("Filas afectadas: " + filasAfectadas);
            preparedStatement.close();

            Statement sentencia = connection.createStatement();
            String departamentoSQL = " select codigo_departamento, nombre , codigo_locacion" +
                    " from departamento";
            ResultSet resultado = sentencia.executeQuery(departamentoSQL);

            while (resultado.next()){
                System.out.println("nombre: "+ resultado.getString("nombre"));
                System.out.println("locación: "+ resultado.getInt("codigo_locacion"));
            }
            connection.commit();
            resultado.close();
            sentencia.close();
            connection.close();

        } catch (SQLException throwables) {
            connection.rollback();
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            connection.rollback();
            e.printStackTrace();
        }finally {
            connection.close();
        }
    }
}
