package com.ventas.ventasweb.servicio;

import com.ventas.ventasweb.dto.Marca;
import com.ventas.ventasweb.dto.Producto;
import com.ventas.ventasweb.dto.Usuario;

import java.util.List;

public interface Servicio {
    public List<Producto> obtenerProductos();
    public Producto obtenerProducto(Producto producto);
    public Marca agregarMarca(Marca marca);
    public Producto agregarProducto(Producto producto);
    public Producto modificarProducto(Producto producto);
    public Usuario agregarUsuario(Usuario usuario);
    public Usuario obtenerUsuario(Usuario usuario);
}
