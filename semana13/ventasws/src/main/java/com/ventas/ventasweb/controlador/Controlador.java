package com.ventas.ventasweb.controlador;

import com.ventas.ventasweb.dto.Marca;
import com.ventas.ventasweb.dto.Producto;
import com.ventas.ventasweb.dto.RespuestaProducto;
import com.ventas.ventasweb.dto.Usuario;
import com.ventas.ventasweb.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;
    @RequestMapping(
            value = "/obtener-productos",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaProducto obtenerProductos(){
        RespuestaProducto respuestaProducto = new RespuestaProducto();
        respuestaProducto.setLista(servicio.obtenerProductos());
        return respuestaProducto;
    }
    @RequestMapping(
            value = "/obtener-producto",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Producto obtenerProducto(@RequestBody Producto producto){
        return servicio.obtenerProducto(producto);
    }
    @RequestMapping(
            value = "/agregar-marca",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Marca agregarMarca(@RequestBody Marca marca){
        return servicio.agregarMarca(marca);
    }
    @RequestMapping(
            value = "/agregar-usuario",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario agregarUsuario(@RequestBody Usuario usuario){
        return servicio.agregarUsuario(usuario);
    }

    @RequestMapping(
            value = "/autenticar-usuario",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario obtenerUsuario(@RequestBody Usuario usuario){
        return servicio.obtenerUsuario(usuario);
    }
}
