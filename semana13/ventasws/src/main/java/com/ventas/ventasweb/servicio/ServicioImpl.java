package com.ventas.ventasweb.servicio;

import com.ventas.ventasweb.dao.Dao;
import com.ventas.ventasweb.dto.Marca;
import com.ventas.ventasweb.dto.Producto;
import com.ventas.ventasweb.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public List<Producto> obtenerProductos() {
        return dao.obtenerProductos();
    }

    public Producto obtenerProducto(Producto producto) {
        return dao.obtenerProducto(producto);
    }

    public Marca agregarMarca(Marca marca) {
        return dao.agregarMarca(marca);
    }


    public Usuario agregarUsuario(Usuario usuario) {
        return dao.agregarUsuario(usuario);
    }
    public Usuario obtenerUsuario(Usuario usuario) {
        return dao.obtenerUsuario(usuario);
    }

    public Producto agregarProducto(Producto producto) {
        return null;
    }

    public Producto modificarProducto(Producto producto) {
        return null;
    }
}
