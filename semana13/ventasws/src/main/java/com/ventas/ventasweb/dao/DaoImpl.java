package com.ventas.ventasweb.dao;

import com.ventas.ventasweb.dto.Marca;
import com.ventas.ventasweb.dto.Producto;
import com.ventas.ventasweb.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public List<Producto> obtenerProductos() {
        List<Producto> lista = new ArrayList<>();
        String sql = " select p.id_producto, p.id_marca, p.nombre nombreProducto, p.peso, p.precio_regular, p.precio_oferta,\n" +
                "       p.oferta, p.calificacion, p.url_imagen, m.nombre nombreMarca\n" +
                " from producto p\n" +
                " join marca m on p.id_marca = m.id_marca";
        try {
            obtenerConexion();
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()){
                lista.add(extraerProducto(resultado));
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }
    private Producto extraerProducto(ResultSet resultado) throws SQLException {
        Producto producto = new Producto(
                resultado.getInt("id_producto"),
                new Marca(
                        resultado.getInt("id_marca"),
                        resultado.getString("nombreMarca")
                )  ,
                resultado.getString("nombreProducto"),
                resultado.getDouble("peso"),
                resultado.getDouble("precio_regular"),
                resultado.getDouble("precio_oferta"),
                "1".equals(resultado.getString("oferta")) ? true : false,
                resultado.getInt("calificacion"),
                resultado.getString("url_imagen")
        );
        return producto;
    }

    public Producto obtenerProducto(Producto producto) {

        String sql = " select p.id_producto, p.id_marca, p.nombre nombreProducto, p.peso, p.precio_regular, p.precio_oferta,\n" +
                "       p.oferta, p.calificacion, p.url_imagen, m.nombre nombreMarca\n" +
                " from producto p\n" +
                " join marca m on p.id_marca = m.id_marca"+
                " where p.id_producto = ?";
                //" where p.nombre like ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, producto.getId());
            //sentencia.setString(1, "%"+producto.getNombre()+"%");
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()){
                producto = extraerProducto(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return producto;
    }

    public Marca agregarMarca(Marca marca) {
        String sql = " insert into marca(id_marca, nombre, fecha ) values (nextval(secuencia_marca),?,CURRENT_TIMESTAMP())";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, marca.getNombre());
            sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return marca;
    }

    public Usuario agregarUsuario(Usuario usuario) {
        String sql = " insert into usuario(id_usuario, nombres, apellidos, correo, clave ) values (nextval(secuencia_usuario),?,?,?,?)";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuario.getNombres());
            sentencia.setString(2, usuario.getApellidos());
            sentencia.setString(3, usuario.getCorreo());
            sentencia.setString(4, usuario.getClave());
            sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }

    public Usuario obtenerUsuario(Usuario usuario) {
        String sql = " SELECT id_usuario, nombres, apellidos, correo, clave\n" +
                " FROM usuario\n" +
                " WHERE correo = ? AND clave = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuario.getCorreo());
            sentencia.setString(2, usuario.getClave());
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()){
                usuario = extraerUsuario(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }
    private Usuario extraerUsuario(ResultSet resultado) throws SQLException {
        Usuario usuario = new Usuario(
                resultado.getInt("id_usuario"),
                resultado.getString("nombres"),
                resultado.getString("apellidos"),
                resultado.getString("correo"),
                null
        );
        return usuario;
    }
    public Producto agregarProducto(Producto producto) {
        return null;
    }

    public Producto modificarProducto(Producto producto) {
        return null;
    }
}
