CREATE TABLE producto (
                          id_producto NUMERIC(9) NOT NULL,
                          id_marca NUMERIC(9) NOT NULL,
                          nombre VARCHAR(100) NOT NULL,
                          peso NUMERIC(5,2) NOT NULL,
                          precio_regular NUMERIC(7,2) NOT NULL,
                          precio_oferta NUMERIC(7,2) NOT NULL,
                          oferta CHAR(1) NOT NULL,
                          calificacion NUMERIC(1) NOT NULL,
                          url_imagen VARCHAR(200) NOT NULL,
                          PRIMARY KEY (id_producto)
);


CREATE TABLE marca (
                       id_marca NUMERIC(9) NOT NULL,
                       nombre VARCHAR(100) NOT NULL,
                       PRIMARY KEY (id_marca)
);

insert into marca(id_marca, nombre) values (1,'Audio-Technica');
insert into marca(id_marca, nombre) values (2,'Edifier');
insert into marca(id_marca, nombre) values (3,'Ultimate Ears');
insert into marca(id_marca, nombre) values (4,'Astell&Kern');

insert into producto(id_producto, id_marca, nombre, peso, precio_regular, precio_oferta, oferta, calificacion, url_imagen)
values (1,1,'Audio-Technica ATN3600L Aguja de Repuesto',0.10, 30,20, '1',0, 'https://cdn11.bigcommerce.com/s-u0vrzs/images/stencil/608x608/products/1096/8364/atn3600l_02__17854.1609969082.jpg?c=2');

insert into producto(id_producto, id_marca, nombre, peso, precio_regular, precio_oferta, oferta, calificacion, url_imagen)
values (2,2,'Edifier R1280Ts Parlante Activo Estudio Mac PC',6.00, 205,175, '1',0, 'https://cdn11.bigcommerce.com/s-u0vrzs/images/stencil/608x608/products/1090/8380/edifier-r1280ts-altavoces-estanteria__37405.1610664723.jpg?c=2');

insert into producto(id_producto, id_marca, nombre, peso, precio_regular, precio_oferta, oferta, calificacion, url_imagen)
values (3,3,'Ultimate Ears Wonderboom 2 - Parlante Bluetooth Portátil Acuático',0.50, 120,100, '1',1, 'https://cdn11.bigcommerce.com/s-u0vrzs/images/stencil/608x608/products/1064/8078/984-001554__46031.1593515870.jpg?c=2');

insert into producto(id_producto, id_marca, nombre, peso, precio_regular, precio_oferta, oferta, calificacion, url_imagen)
values (4,4,'Astell&Kern SA700 Reproductor Portátil Hi-Res',0.30, 1465,1245, '1',3, 'https://cdn11.bigcommerce.com/s-u0vrzs/images/stencil/608x608/products/1061/8059/AK_SA700_1__66299.1592356065.jpg?c=2');

select * from marca;

select p.id_producto, p.id_marca, p.nombre nombreProducto, p.peso, p.precio_regular, p.precio_oferta,
       p.oferta, p.calificacion, p.url_imagen, m.nombre nombreMarca
from producto p
join marca m on p.id_marca = m.id_marca
where p.id_producto = 1;

