import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Producto, Usuario } from '../interfaces';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {
  correo: string = '';
  nombres: string = '';
  apellidos: string = '';
  credencial: string = '';
  credencialRepeticion: string = '';
  mensaje: string = '';
  idProducto: string = '';
  lista: Producto[] = [];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.obtenerProductos().subscribe( respuesta => {
      this.lista = respuesta.lista;
    });
  }
  registrar(): void{
    console.log(this.correo);
    console.log(this.apellidos);
    console.log(this.nombres);
    console.log(this.credencial);
    console.log(this.credencialRepeticion);
    console.log(this.idProducto);
    if(this.credencial == this.credencialRepeticion){
      const usuario: Usuario = {
        id: undefined,
        nombres: this.nombres,
        apellidos: this.apellidos,
        correo: this.correo,
        clave: this.credencial
      };
      this.api.agregarUsuario(usuario).subscribe( retorno =>{
        this.mensaje = 'El usuario ' + retorno.nombres + ' fue registrado correctamente';
      });
    }
    else{
      this.mensaje = 'La contraseña no es la misma';
    }
    
  }

}
