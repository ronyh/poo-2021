package com.example.finalws.dao;

import com.example.finalws.dto.Usuario;

import java.util.List;


public interface Dao {
    public List<Usuario> obtenerUsuarios(Usuario usuario);
    public Usuario autenticarUsuario(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
}
