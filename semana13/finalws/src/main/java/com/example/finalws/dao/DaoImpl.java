package com.example.finalws.dao;

import com.example.finalws.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public List<Usuario> obtenerUsuarios(Usuario usuario) {
        List<Usuario> lista = new ArrayList<>();
        String SQL = "select id_usuario, nombres, apellidos, correo, administrador, clave\n" +
                "from usuario\n" +
                "where administrador != '1' and\n" +
                "      (\n";
        if(usuario.getNombres() != null && !usuario.getNombres().trim().equals("")){
            SQL += "          upper(nombres) like ?\n";
        }
        if(usuario.getApellidos() != null && !usuario.getApellidos().trim().equals("")){
            SQL += "          or upper(apellidos) like ?\n";
        }
        if(usuario.getCorreo() != null && !usuario.getCorreo().trim().equals("")){
            SQL += "          or upper(correo) like ?\n";
        }
        SQL += "    )";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            Integer indice = 1;
            if(usuario.getNombres() != null && !usuario.getNombres().trim().equals("")){
                sentencia.setString(indice, "%" + usuario.getNombres().toUpperCase() + "%");
                indice++;
            }
            if(usuario.getApellidos() != null && !usuario.getApellidos().trim().equals("")){
                sentencia.setString(indice, "%" + usuario.getApellidos().toUpperCase() + "%");
                indice++;
            }
            if(usuario.getCorreo() != null && !usuario.getCorreo().trim().equals("")) {
                sentencia.setString(indice, "%" + usuario.getCorreo().toUpperCase() + "%");
            }
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                Usuario p = extraerUsuario(resultado);
                lista.add(p);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }
    private Usuario extraerUsuario(ResultSet resultado) throws SQLException {
        Usuario usuario = new Usuario(
                resultado.getInt("id_usuario"),
                resultado.getString("nombres"),
                resultado.getString("apellidos"),
                resultado.getString("correo"),
                resultado.getString("administrador"),
                resultado.getString("clave")
        );
        return usuario;
    }

    public Usuario autenticarUsuario(Usuario usuario) {
        String SQL = "select id_usuario, nombres, apellidos, correo, administrador, clave\n" +
                "from usuario\n" +
                "where upper(correo) = ? and clave = ?";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1, usuario.getCorreo().toUpperCase());
            sentencia.setString(2, usuario.getClave());

            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                usuario = extraerUsuario(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }


    public Usuario actualizarUsuario(Usuario usuario) {
        String SQL = "update usuario\n" +
                "set\n" +
                "    nombres = ?,\n" +
                "    apellidos = ?,\n" +
                "    correo = ?\n" +
                "where id_usuario = ? and administrador != '1'";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1, usuario.getNombres());
            sentencia.setString(2, usuario.getApellidos());
            sentencia.setString(3, usuario.getCorreo());
            sentencia.setInt(4, usuario.getId());
            sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }
}
