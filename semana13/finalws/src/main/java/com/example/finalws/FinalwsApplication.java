package com.example.finalws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalwsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalwsApplication.class, args);
    }

}
