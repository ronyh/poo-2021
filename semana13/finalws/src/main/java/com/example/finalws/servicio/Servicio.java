package com.example.finalws.servicio;


import com.example.finalws.dto.Usuario;

import java.util.List;

public interface Servicio {
    public List<Usuario> obtenerUsuarios(Usuario usuario);
    public Usuario autenticarUsuario(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
}
