package com.example.finalws.servicio;


import com.example.finalws.dao.Dao;
import com.example.finalws.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public List<Usuario> obtenerUsuarios(Usuario usuario){
        return dao.obtenerUsuarios(usuario);
    }
    public Usuario autenticarUsuario(Usuario usuario){
        return dao.autenticarUsuario(usuario);
    }
    public Usuario actualizarUsuario(Usuario usuario){
        return dao.actualizarUsuario(usuario);
    }
}
