package com.example.finalws.controlador;

import com.example.finalws.dto.RespuestaUsuario;
import com.example.finalws.dto.Usuario;
import com.example.finalws.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;
    @RequestMapping(
        value = "obtener-usuarios",method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaUsuario obtenerUsuarios(@RequestBody Usuario usuario){
        RespuestaUsuario respuestaUsuario = new RespuestaUsuario();
        respuestaUsuario.setLista(servicio.obtenerUsuarios(usuario));
        return respuestaUsuario;
    }
    @RequestMapping(
            value = "autenticar-usuario",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario autenticarUsuario(@RequestBody Usuario usuario){
        return servicio.autenticarUsuario(usuario);
    }
    @RequestMapping(
            value = "actualizar-usuario",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario actualizarUsuario(@RequestBody Usuario usuario){
        return servicio.actualizarUsuario(usuario);
    }
}
