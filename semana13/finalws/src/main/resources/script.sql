create table usuario(
                        id_usuario numeric(9,0) primary key,
                        nombres varchar(100),
                        apellidos varchar(100),
                        correo varchar(200) unique,
                        administrador char(1),
                        clave varchar(200)
);

insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave) values (1,'Rony','Hancco','rony@gmail.com','1','JAVAocho');
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave) values (2,'Jean','Fiori','jean@gmail.com','0',null);
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave) values (3,'Laura','Garret','laura@gmail.com','0',null);

select id_usuario, nombres, apellidos, correo, administrador, clave
from usuario
where correo = 'rony@gmail.com' and clave = 'JAVAocho';

select id_usuario, nombres, apellidos, correo, administrador, clave
from usuario
where administrador != '1' and
      (
          nombres like '%rony%'
          or apellidos like '%hancco%'
          or correo like '%rony%'
    );

update usuario
set
    nombres = 'Rony',
    apellidos = 'Hancco',
    correo = 'rony@gmail.com'
where id_usuario = 1 and administrador != '1';