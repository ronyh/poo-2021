package com.proyecto.semana6.seminarioparcial.patrones.builder;

import java.util.ArrayList;
import java.util.List;

public class MonedaSecundaria implements Builder{
    private List<String> material;
    private String sello;
    public MonedaSecundaria() {
        this.material = new ArrayList<>();
    }

    public void recolectarMaterial() {
        material.add("Zinc 150g");
        material.add("Hierro 50g");
        material.add("Cobre 50g");
        material.add("Plata 250g");
    }

    public void acuniarMoneda() {
        this. sello = "Bicentenario";
    }

    public Double actualizarSaldo() {
        return 2500.00;
    }
    public void mostrarComposicion(){
        int indicador = 1;
        for (String mineral: material) {
            System.out.println(indicador+" "+mineral);
            indicador++;
        }
    }
}

