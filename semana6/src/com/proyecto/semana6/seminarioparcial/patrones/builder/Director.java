package com.proyecto.semana6.seminarioparcial.patrones.builder;

public class Director {
    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }
    public void crearMoneda(){
        builder.recolectarMaterial();
        builder.acuniarMoneda();
        System.out.println(builder.actualizarSaldo());
        builder.mostrarComposicion();
    }
}
