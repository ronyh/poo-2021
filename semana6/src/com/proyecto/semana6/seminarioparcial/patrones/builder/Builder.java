package com.proyecto.semana6.seminarioparcial.patrones.builder;

public interface Builder {
    void recolectarMaterial();
    void acuniarMoneda();
    Double actualizarSaldo();
    void mostrarComposicion();
}
