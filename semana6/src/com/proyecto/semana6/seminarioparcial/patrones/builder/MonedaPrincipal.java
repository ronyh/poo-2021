package com.proyecto.semana6.seminarioparcial.patrones.builder;

import java.util.ArrayList;
import java.util.List;

public class MonedaPrincipal implements Builder{
    private List<String> material;
    private String sello;
    public MonedaPrincipal() {
        this.material = new ArrayList<>();
    }

    public void recolectarMaterial() {
        material.add("Zinc 50g");
        material.add("Hierro 150g");
        material.add("Cobre 5g");
    }

    public void acuniarMoneda() {
        this. sello = "Cinco soles";
    }

    public Double actualizarSaldo() {
        return 5.00;
    }
    public void mostrarComposicion(){
        for (String mineral: material) {
            System.out.println(mineral);
        }
    }
}
