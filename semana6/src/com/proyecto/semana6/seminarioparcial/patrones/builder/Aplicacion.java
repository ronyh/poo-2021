package com.proyecto.semana6.seminarioparcial.patrones.builder;

public class Aplicacion {
    public static void main(String[] args) {
        Builder builder = new MonedaSecundaria();
        generarMoneda(builder);

        Builder builder2 = new MonedaPrincipal();
        generarMoneda(builder2);
    }
    private static void generarMoneda(Builder builder){
        Director director = new Director(builder);
        director.crearMoneda();
    }
}
