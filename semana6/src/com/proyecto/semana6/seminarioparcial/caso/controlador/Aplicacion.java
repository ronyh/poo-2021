package com.proyecto.semana6.seminarioparcial.caso.controlador;

import com.proyecto.semana6.seminarioparcial.caso.dto.Actor;
import com.proyecto.semana6.seminarioparcial.caso.dto.Dvd;
import com.proyecto.semana6.seminarioparcial.caso.dto.Pelicula;
import com.proyecto.semana6.seminarioparcial.caso.enumeraciones.Categoria;
import com.proyecto.semana6.seminarioparcial.caso.excepciones.SinTituloExcepcion;
import com.proyecto.semana6.seminarioparcial.caso.servicio.Servicio;
import com.proyecto.semana6.seminarioparcial.caso.servicio.ServicioImpl;
import com.proyecto.semana6.seminarioparcial.caso.servicio.SingletonServicio;

import java.util.*;

public class Aplicacion {
    public static void main(String[] args) {
        Map<Map<Integer,Integer>,String> mapa =new HashMap<>();

        Map<Integer,Integer> mapita = new HashMap<>();
        mapita.put(1,5000);
        mapa.put(mapita,"Rony");

        Set<Actor> conjunto = new HashSet<>();
        Actor actor1 = new Actor();
        actor1.setNombre("Branlon");
        Actor actor2 = new Actor();
        actor2.setNombre("Branlon");
        conjunto.add(actor1);
        conjunto.add(actor2);

        for (Actor actor: conjunto) {
            System.out.println(actor.getNombre());
        }
        Actor[] actores = (Actor[])conjunto.toArray();

        Map<String, List<Pelicula> > mapaPelicula = new HashMap<>();
        List<Pelicula> peliculasComedia = new ArrayList<>();
        List<Pelicula> peliculasMusical = new ArrayList<>();
        try {
            Pelicula sol = new Pelicula("sol");
            Pelicula luna = new Pelicula("luna");
            peliculasComedia.add(sol);
            peliculasComedia.add(luna);

            Pelicula estrellas = new Pelicula("estrellas");
            Pelicula mar = new Pelicula("mar");
            peliculasMusical.add(estrellas);
            peliculasMusical.add(mar);
        }catch (SinTituloExcepcion excepcion){
            System.out.println("Se ingreso una película sin título");
        }
        System.out.println(peliculasMusical.get(0).getTitulo()+" antes");

        mapaPelicula.put("comedia", peliculasComedia);
        mapaPelicula.put("musical", peliculasMusical);

        List<Pelicula> lista = mapaPelicula.get("musical");
        for (Pelicula ejemplar: lista) {
            System.out.println(ejemplar.getTitulo());
        }

        List<Pelicula> peliculas = new ArrayList<>();
        Pelicula pelicula = new Pelicula();
        pelicula.setCopias(20);
        pelicula.setTitulo("Aprobando los parciales");
        pelicula.setCategoria(Categoria.SUSPENSO);
        pelicula.setIdentificador(1);

        Dvd dvdParciales = new Dvd();
        dvdParciales.setNumero(1);
        dvdParciales.setPelicula(pelicula);

        Dvd dvdParciales2 = new Dvd();
        dvdParciales2.setNumero(2);
        dvdParciales2.setPelicula(pelicula);

        Dvd dvdParciales3 = new Dvd();
        dvdParciales3.setNumero(3);
        dvdParciales3.setPelicula(pelicula);

        List<Dvd> dvdLista = new ArrayList<>();
        dvdLista.add(dvdParciales);
        dvdLista.add(dvdParciales2);
        dvdLista.add(dvdParciales3);

        pelicula.setDvds(dvdLista);
        peliculas.add(pelicula);

        Servicio servicio = SingletonServicio.getServicio();
        List<Pelicula> listaMapa = servicio.buscarPeliculas(peliculas,"parcial");
        for (Pelicula pelicula1: listaMapa) {
            System.out.println(pelicula1.getIdentificador());
        }

        //servicio.alquilarDvd();

    }
}
