package com.proyecto.semana6.seminarioparcial.caso.enumeraciones;

public enum Categoria {
    COMEDIA("Comedia"),SUSPENSO("Suspenso");
    private String nombre;

    Categoria(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
}
