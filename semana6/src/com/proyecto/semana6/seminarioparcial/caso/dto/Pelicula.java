package com.proyecto.semana6.seminarioparcial.caso.dto;

import com.proyecto.semana6.seminarioparcial.caso.enumeraciones.Categoria;
import com.proyecto.semana6.seminarioparcial.caso.excepciones.SinTituloExcepcion;

import java.util.List;

public class Pelicula {
    private Integer identificador;
    private String titulo;
    private Categoria categoria;
    private Integer copias;
    private List<Dvd> dvds;
    private List<Actor> protagonistas;

    public Pelicula() {
    }

    public Pelicula(String titulo) throws SinTituloExcepcion{
        if(titulo == null){
            throw new SinTituloExcepcion("No hay título");
        }
        this.titulo = titulo;
    }

    public Integer getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador = identificador;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Integer getCopias() {
        return copias;
    }

    public void setCopias(Integer copias) {
        this.copias = copias;
    }

    public List<Dvd> getDvds() {
        return dvds;
    }

    public void setDvds(List<Dvd> dvds) {
        this.dvds = dvds;
    }

    public List<Actor> getProtagonistas() {
        return protagonistas;
    }

    public void setProtagonistas(List<Actor> protagonistas) {
        this.protagonistas = protagonistas;
    }
}
