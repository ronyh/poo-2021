package com.proyecto.semana6.seminarioparcial.caso.dto;

import java.util.List;

public class Socio {
    private Integer membresia;
    private String nombres;
    private String apellidos;
    private String telefono;
    private String direccion;
    private List<Dvd> alquilados;

    public Integer getMembresia() {
        return membresia;
    }

    public void setMembresia(Integer membresia) {
        this.membresia = membresia;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Dvd> getAlquilados() {
        return alquilados;
    }

    public void setAlquilados(List<Dvd> alquilados) {
        this.alquilados = alquilados;
    }
}
