package com.proyecto.semana6.seminarioparcial.caso.dto;
//DTO data transfer object
public class Dvd {
    private Integer numero;
    private Pelicula pelicula;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }
}
