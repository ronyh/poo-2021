package com.proyecto.semana6.seminarioparcial.caso.dto;

public class Actor {
    private String nombre;
    private String nombreReal;
    private String fechaNacimiento;

    public int hashCode() {
        return this.nombre.hashCode();
    }

    public boolean equals(Object obj) {
        Actor actor = (Actor) obj;
        return this.nombre.equals(actor.getNombre());
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreReal() {
        return nombreReal;
    }

    public void setNombreReal(String nombreReal) {
        this.nombreReal = nombreReal;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
}
