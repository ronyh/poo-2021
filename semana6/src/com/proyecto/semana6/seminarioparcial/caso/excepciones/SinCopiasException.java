package com.proyecto.semana6.seminarioparcial.caso.excepciones;

public class SinCopiasException extends Exception{
    public SinCopiasException(String message) {
        super(message);
    }
}
