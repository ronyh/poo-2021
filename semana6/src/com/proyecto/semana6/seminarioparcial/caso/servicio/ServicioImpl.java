package com.proyecto.semana6.seminarioparcial.caso.servicio;

import com.proyecto.semana6.seminarioparcial.caso.dto.Dvd;
import com.proyecto.semana6.seminarioparcial.caso.dto.Pelicula;
import com.proyecto.semana6.seminarioparcial.caso.dto.Socio;
import com.proyecto.semana6.seminarioparcial.caso.excepciones.SinCopiasException;

import java.util.ArrayList;
import java.util.List;

public class ServicioImpl implements Servicio{
    ServicioImpl() {
    }

    public List<Pelicula> buscarPeliculas(List<Pelicula> lista, String titulo) {
        List<Pelicula> listaNueva = new ArrayList<>();
        for (Pelicula pelicula: lista) {
            if(pelicula.getTitulo().contains(titulo)){
                listaNueva.add(pelicula);
            }
        }
        return listaNueva;
    }

    public void alquilarDvd(Socio socio, Dvd dvd) throws SinCopiasException {
        socio.getAlquilados().add(dvd);
        if(dvd.getPelicula().getCopias()<1){
            throw new SinCopiasException("No se dispone de copias");
        }
        dvd.getPelicula().setCopias(dvd.getPelicula().getCopias() -1 );
    }
}
