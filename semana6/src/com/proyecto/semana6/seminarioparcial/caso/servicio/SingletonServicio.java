package com.proyecto.semana6.seminarioparcial.caso.servicio;

public abstract class SingletonServicio {
    private static Servicio servicio;
    public static Servicio getServicio(){
        if(servicio == null){
            servicio = new ServicioImpl();
        }
        return servicio;
    }
}
