package com.proyecto.semana6.seminarioparcial.caso.servicio;

import com.proyecto.semana6.seminarioparcial.caso.dto.Dvd;
import com.proyecto.semana6.seminarioparcial.caso.dto.Pelicula;
import com.proyecto.semana6.seminarioparcial.caso.dto.Socio;
import com.proyecto.semana6.seminarioparcial.caso.excepciones.SinCopiasException;

import java.util.List;

public interface Servicio {
    List<Pelicula> buscarPeliculas(List<Pelicula> lista, String titulo);
    void alquilarDvd(Socio socio, Dvd dvd) throws SinCopiasException;
}
