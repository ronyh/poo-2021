package com.proyecto.semana6.seminarioparcial.excepciones;

public class MiExcepcion extends Exception{
    public MiExcepcion(String message) {
        super(message);
    }
}
