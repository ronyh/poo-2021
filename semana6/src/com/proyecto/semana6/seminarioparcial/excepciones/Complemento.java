package com.proyecto.semana6.seminarioparcial.excepciones;

public class Complemento {
    public void imprimir() throws MiExcepcion{
        throw new MiExcepcion("No hay complemento");
    }
}
