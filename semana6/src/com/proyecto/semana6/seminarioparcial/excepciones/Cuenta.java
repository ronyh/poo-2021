package com.proyecto.semana6.seminarioparcial.excepciones;

public class Cuenta {
    public void iniciar() throws MiExcepcion{
        Complemento complemento = new Complemento();
        try {
            complemento.imprimir();
        } catch (MiExcepcion miExcepcion) {
            miExcepcion.printStackTrace();
            throw miExcepcion;
        }
    }
}
