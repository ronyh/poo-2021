package com.proyecto.semana6.seminarioparcial.excepciones;

public class Aplicacion {
    public static void main(String[] args) {
        Cuenta cuenta = new Cuenta();
        try {
            cuenta.iniciar();
        } catch (MiExcepcion miExcepcion) {
            miExcepcion.printStackTrace();
        }
    }
}
