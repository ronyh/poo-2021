package com.proyecto.semana6.excepciones;

public class ExcepcionNegativo extends Exception{
    public ExcepcionNegativo() {
        super("Números negativos no son admitidos");
    }
}
