package com.proyecto.semana6.factory.method;

public class CreadorClienteCine extends CreadorCliente{
    public Cliente fabricarCliente() {
        Cliente cliente = new ClienteImpl();
        return cliente;
    }
}
