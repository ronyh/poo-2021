package com.proyecto.semana6.factory.method;

public abstract class CreadorCliente {
    public abstract Cliente fabricarCliente();
}
