package com.proyecto.semana6.factory.aplicacion;

import com.proyecto.semana6.factory.method.Cliente;
import com.proyecto.semana6.factory.method.ClienteImpl;
import com.proyecto.semana6.factory.method.CreadorCliente;
import com.proyecto.semana6.factory.method.CreadorClienteCine;

public class Aplicacion {
    public static void main(String[] args) {
        CreadorCliente creadorCliente = new CreadorClienteCine();
        Cliente cliente = creadorCliente.fabricarCliente();
    }
}
