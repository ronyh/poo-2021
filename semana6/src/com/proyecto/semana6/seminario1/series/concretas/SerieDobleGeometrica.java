package com.proyecto.semana6.seminario1.series.concretas;

import com.proyecto.semana6.seminario1.series.abstractas.Serie;

public class SerieDobleGeometrica extends Serie {

    public SerieDobleGeometrica(Integer terminoEnesimo) {
        super(terminoEnesimo);
    }

    public void generarTerminos() {
        this.terminos.add(1);
        for (int i = 1; i < this.terminoEnesimo; i++) {
            this.terminos.add( 2 * this.terminos.get(i-1) );
        }
    }
}
