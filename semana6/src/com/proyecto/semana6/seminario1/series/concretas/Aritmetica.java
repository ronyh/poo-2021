package com.proyecto.semana6.seminario1.series.concretas;

import com.proyecto.semana6.seminario1.series.abstractas.Serie;

public class Aritmetica extends Serie {
    public Aritmetica(Integer terminoEnesimo) {
        super(terminoEnesimo);
    }
    public void generarTerminos() {
        for (int i = 0; i < this.terminoEnesimo; i++) {
            this.terminos.add( i + 1);
        }
    }
}
