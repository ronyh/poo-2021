package com.proyecto.semana6.seminario1.series.concretas;

import com.proyecto.semana6.seminario1.series.abstractas.Serie;

public class Fibonacci extends Serie {
    public Fibonacci(Integer terminoEnesimo) {
        super(terminoEnesimo);
    }

    public void generarTerminos() {
        if(this.terminoEnesimo == 1){
            this.terminos.add(1);
        }
        else if(this.terminoEnesimo > 1){
            this.terminos.add(1);
            this.terminos.add(1);
        }

        for (int i = 2; i < this.terminoEnesimo; i++) {
            this.terminos.add(
                    this.terminos.get(i-2)+ this.terminos.get(i-1)
                    );
        }
    }

}
