package com.proyecto.semana6.seminario1.series.controlador;

import com.proyecto.semana6.seminario1.series.abstractas.Serie;
import com.proyecto.semana6.seminario1.series.concretas.Aritmetica;
import com.proyecto.semana6.seminario1.series.concretas.Fibonacci;
import com.proyecto.semana6.seminario1.series.concretas.SerieDobleGeometrica;

public class Aplicacion {
    public static void main(String[] args) {
        //Serie serie = new Fibonacci(10);
        //Serie serie = new Aritmetica(10);
        Serie serie = new SerieDobleGeometrica(10);
        serie.procesarSerie();
        System.out.println(serie);

    }

}
