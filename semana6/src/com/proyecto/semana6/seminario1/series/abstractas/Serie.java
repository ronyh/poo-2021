package com.proyecto.semana6.seminario1.series.abstractas;

import java.util.ArrayList;
import java.util.List;

public abstract class Serie {
    protected List<Integer> terminos;
    protected Integer terminoEnesimo;
    public Serie(Integer terminoEnesimo) {
        this.terminos = new ArrayList<Integer>();
        this.terminoEnesimo = terminoEnesimo;
    }

    public void procesarSerie(){
        generarTerminos();
        System.out.println(calcularSumaTerminos());
        System.out.println(calcularPromedioTerminos());
    }
    public abstract void generarTerminos();

    public Integer calcularSumaTerminos() {
        Integer suma = 0;
        for (Integer item: terminos) {
            suma += item;
        }
        return suma;
    }

    public Double calcularPromedioTerminos() {
        Double suma = calcularSumaTerminos().doubleValue();
        return suma/terminoEnesimo;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Serie { " +
                "terminos = ");
        for ( Integer item: terminos) {
            sb.append(item+ " ");
        }
        sb.append(", terminoEnesimo = " + terminoEnesimo +
                '}');
        return sb.toString();
    }
}
