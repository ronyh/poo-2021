package com.proyecto.semana6.repaso.notascurso.servicio;

public abstract class SingletonAlumnoServicio {
    private static AlumnoServicio alumnoServicio;
    public static AlumnoServicio getInstancia(){
        if(alumnoServicio == null){
            alumnoServicio = new AlumnoServicioImpl();
        }
        return alumnoServicio;
    }
}
