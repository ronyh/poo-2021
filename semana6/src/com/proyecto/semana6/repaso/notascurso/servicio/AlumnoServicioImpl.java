package com.proyecto.semana6.repaso.notascurso.servicio;

import com.proyecto.semana6.repaso.notascurso.dto.Alumno;
import com.proyecto.semana6.repaso.notascurso.dto.Curso;
import com.proyecto.semana6.repaso.notascurso.dto.Evaluacion;

public class AlumnoServicioImpl implements AlumnoServicio{
    AlumnoServicioImpl() {
    }

    public void procesarEvaluaciones(Alumno alumno) {
        for ( Curso curso: alumno.getCursos()) {
            curso.procesarNota();
            for (Evaluacion evaluacion: curso.getEvaluaciones()) {
                evaluacion.procesarNota();
            }
        }
    }
}
