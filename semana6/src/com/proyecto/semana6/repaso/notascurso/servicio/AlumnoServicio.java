package com.proyecto.semana6.repaso.notascurso.servicio;

import com.proyecto.semana6.repaso.notascurso.dto.Alumno;

public interface AlumnoServicio {
    void procesarEvaluaciones(Alumno alumno);
}
