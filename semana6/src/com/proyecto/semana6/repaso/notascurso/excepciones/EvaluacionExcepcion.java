package com.proyecto.semana6.repaso.notascurso.excepciones;

public class EvaluacionExcepcion extends Exception{
    public EvaluacionExcepcion(String message) {
        super(message);
    }
}
