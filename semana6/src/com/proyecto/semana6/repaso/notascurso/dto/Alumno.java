package com.proyecto.semana6.repaso.notascurso.dto;

import java.util.List;

public class Alumno {
    private String codigo;
    private String nombre;
    private List<Curso> cursos;

    public Alumno(String codigo, String nombre, List<Curso> cursos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cursos = cursos;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public int hashCode() {
        return this.codigo.hashCode();
    }


    public boolean equals(Object obj) {
        Alumno alumno = (Alumno) obj;
        return this.codigo.equals(alumno.getCodigo());
    }
}
