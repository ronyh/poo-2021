package com.proyecto.semana6.repaso.notascurso.dto;

import com.proyecto.semana6.repaso.notascurso.enumeraciones.SistemaEvaluacion;
import com.proyecto.semana6.repaso.notascurso.excepciones.EvaluacionExcepcion;

public class Evaluacion {
    private Integer nota;
    private String tipo;
    private Boolean aprobado;
    /*
    public Evaluacion(Integer nota, String tipo) {
        try{
            if(nota < 0){
                throw new EvaluacionExcepcion("No se permite notas negativas");
            }
            this.nota = nota;
        }catch (EvaluacionExcepcion excepcion){
            excepcion.printStackTrace();
        }
        finally {
            this.tipo = tipo;
            this.aprobado = false;
        }
    }*/

    public Evaluacion(Integer nota, String tipo) throws EvaluacionExcepcion{
        if(nota < 0){
            throw new EvaluacionExcepcion("No se permite notas negativas");
        }
        this.nota = nota;
        this.tipo = tipo;
        this.aprobado = false;

    }

    public void procesarNota(){
        for ( SistemaEvaluacion sistemaEvaluacion : SistemaEvaluacion.values()) {
            if(sistemaEvaluacion.getTipo().equals(tipo)
                && sistemaEvaluacion.getNotaAprobacion()<= nota){
                    this.aprobado = true;
                    break;
            }
        }
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Boolean getAprobado() {
        return aprobado;
    }

}
