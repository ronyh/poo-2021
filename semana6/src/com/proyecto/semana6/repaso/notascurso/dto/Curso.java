package com.proyecto.semana6.repaso.notascurso.dto;

import com.proyecto.semana6.repaso.notascurso.enumeraciones.SistemaEvaluacion;
import com.proyecto.semana6.repaso.notascurso.enumeraciones.TipoEvaluacion;

import java.util.List;

public class Curso {
    private String nombre;
    private List<Evaluacion> evaluaciones;
    private String sistemaEvaluacion;
    private Double nota;

    public Curso(String nombre, List<Evaluacion> evaluaciones, String sistemaEvaluacion) {
        this.nombre = nombre;
        this.evaluaciones = evaluaciones;
        this.sistemaEvaluacion = sistemaEvaluacion;
    }

    public void procesarNota(){
        for ( SistemaEvaluacion sistemaEvaluacion : SistemaEvaluacion.values()) {
            if(sistemaEvaluacion.getTipo().equals(this.sistemaEvaluacion)
            ){
                calcularNotaSistemaEvaluacion();
            }
        }
    }

    private void calcularNotaSistemaEvaluacion(){
        nota = 0d;
        if(sistemaEvaluacion.equals(SistemaEvaluacion.F)){
            calcularNotaPorTipoEvaluacion(16d, 4d, 2d);
        }
        else if(sistemaEvaluacion.equals(SistemaEvaluacion.D)){
            calcularNotaPorTipoEvaluacion(12d, 3d, 3d);
        }
    }

    private void calcularNotaPorTipoEvaluacion(Double pesoPractica, Double pesoParcial, Double pesoFinal){
        for (Evaluacion evaluacion: evaluaciones) {
            for ( TipoEvaluacion tipoEvaluacion: TipoEvaluacion.values()) {
                if(tipoEvaluacion.getTipo().equals(evaluacion.getTipo())){
                    if(tipoEvaluacion.equals(TipoEvaluacion.PRACTICA)){
                        nota += (evaluacion.getNota()/pesoPractica);
                    }
                    else if(tipoEvaluacion.equals(TipoEvaluacion.EXAMEN_PARCIAL)){
                        nota += (evaluacion.getNota()/pesoParcial);
                    }
                    else if(tipoEvaluacion.equals(TipoEvaluacion.EXAMEN_FINAL)){
                        nota += (evaluacion.getNota()/pesoFinal);
                    }
                }
            }
        }
    }
    public String getSistemaEvaluacion() {
        return sistemaEvaluacion;
    }

    public void setSistemaEvaluacion(String sistemaEvaluacion) {
        this.sistemaEvaluacion = sistemaEvaluacion;
    }

    public Double getNota() {
        return nota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Evaluacion> getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(List<Evaluacion> evaluaciones) {
        this.evaluaciones = evaluaciones;
    }
}
