package com.proyecto.semana6.repaso.notascurso.controlador;

import com.proyecto.semana6.repaso.notascurso.dto.Alumno;
import com.proyecto.semana6.repaso.notascurso.dto.Curso;
import com.proyecto.semana6.repaso.notascurso.dto.Evaluacion;
import com.proyecto.semana6.repaso.notascurso.enumeraciones.SistemaEvaluacion;
import com.proyecto.semana6.repaso.notascurso.enumeraciones.TipoEvaluacion;
import com.proyecto.semana6.repaso.notascurso.excepciones.EvaluacionExcepcion;
import com.proyecto.semana6.repaso.notascurso.servicio.AlumnoServicio;
import com.proyecto.semana6.repaso.notascurso.servicio.AlumnoServicioImpl;
import com.proyecto.semana6.repaso.notascurso.servicio.SingletonAlumnoServicio;

import java.util.*;

public class AplicacionControlador {
    public static void main(String[] args) {
        try{
            Evaluacion pc11 = new Evaluacion(12, TipoEvaluacion.PRACTICA.getTipo());
            Evaluacion pc12 = new Evaluacion(18, TipoEvaluacion.PRACTICA.getTipo());
            Evaluacion pc13 = new Evaluacion(12, TipoEvaluacion.PRACTICA.getTipo());
            Evaluacion pc14 = new Evaluacion(19, TipoEvaluacion.PRACTICA.getTipo());
            Evaluacion parcial1 = new Evaluacion(20, TipoEvaluacion.EXAMEN_PARCIAL.getTipo());
            Evaluacion final1 = new Evaluacion(20, TipoEvaluacion.EXAMEN_FINAL.getTipo());
            List<Evaluacion> evaluaciones = new ArrayList<>();
            evaluaciones.add(pc11);
            evaluaciones.add(pc12);
            evaluaciones.add(pc13);
            evaluaciones.add(pc14);
            evaluaciones.add(parcial1);
            evaluaciones.add(final1);

            Curso curso1 = new Curso("Programación orientada a objetos",evaluaciones, SistemaEvaluacion.F.getTipo());
            List<Curso> cursos = new ArrayList<>();
            cursos.add(curso1);

            Alumno alumno = new Alumno("20201524J","Martin Codd",cursos);
            Alumno alumno1 = new Alumno("20201524J","Martin Codd 1",cursos);
            Alumno alumno2 = new Alumno("20201525J","Martin Codd 2",cursos);

            AlumnoServicio servicio = SingletonAlumnoServicio.getInstancia();
            servicio.procesarEvaluaciones(alumno);

            Curso cursoElegido = cursos.get(0);

            Map<String, Alumno> mapaAlumno = new HashMap<>();
            mapaAlumno.put(alumno.getCodigo(),alumno);

            System.out.println( mapaAlumno.get("20201524J").getNombre());

            Set<Alumno> conjuntoAlumno = new HashSet<>();
            conjuntoAlumno.add(alumno);
            conjuntoAlumno.add(alumno1);
            conjuntoAlumno.add(alumno2);
            System.out.println("Uso de set");
            for (Alumno elemento: conjuntoAlumno) {
                System.out.println(elemento.getNombre());
            }

        }catch (EvaluacionExcepcion excepcion){
            excepcion.printStackTrace();
        }catch (Exception excepcion){
            excepcion.printStackTrace();
        }



    }
}
