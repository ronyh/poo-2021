package com.proyecto.semana6.repaso.notascurso.enumeraciones;

public enum SistemaEvaluacion {
    D("D",10),F("F",10);
    private String tipo;
    private Integer notaAprobacion;

    SistemaEvaluacion(String tipo, Integer notaAprobacion) {
        this.tipo = tipo;
        this.notaAprobacion = notaAprobacion;
    }

    public String getTipo() {
        return tipo;
    }

    public Integer getNotaAprobacion() {
        return notaAprobacion;
    }
}
