package com.proyecto.semana6.repaso.notascurso.enumeraciones;

public enum TipoEvaluacion {
    EXAMEN_PARCIAL("EP"), EXAMEN_FINAL("EF"), PRACTICA("P");
    private String tipo;

    TipoEvaluacion(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
}
