package com.proyecto.semana6.evaluacion;

public class Aplicacion {
    public static void main(String[] args) {
        Evaluable evaluacion = new Evaluacion("2da practica",20);

        Calculable pregunta = new Pregunta("¿Qué es herencia?",2);

        Alternativa primera = new Alternativa("Pilar de poo",true);
        Alternativa segunda = new Alternativa("Pilar de pp",false);
        Alternativa tercera = new Alternativa("Pilar de funcional",false);
        Alternativa cuarta = new Alternativa("Pilar de p paralela",false);

        pregunta.agregarAlternativa(primera, segunda, tercera, cuarta);
        evaluacion.agregarPreguntas(pregunta);

        evaluacion.calcularPuntaje();

    }
}
