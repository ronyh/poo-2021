package com.proyecto.semana6.evaluacion;

public interface Calculable {
    public void agregarAlternativa(Alternativa ... alternativas);
    void calcularPuntaje(Alternativa ... alternativas);
    Integer getPuntajeCalculado();
}
