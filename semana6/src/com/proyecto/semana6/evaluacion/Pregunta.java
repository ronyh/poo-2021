package com.proyecto.semana6.evaluacion;

import java.util.ArrayList;
import java.util.List;

public class Pregunta implements Calculable{
    private String enunciado;
    private List<Alternativa> alternativas;
    private Integer puntajeTotal;
    private Integer puntajeCalculado;

    public Pregunta(String enunciado, Integer puntajeTotal) {
        this.enunciado = enunciado;
        this.puntajeTotal = puntajeTotal;
        this.alternativas = new ArrayList<Alternativa>();
        this.puntajeCalculado = 0;
    }
    public void agregarAlternativa(Alternativa ... alternativas){
        for ( Alternativa item: alternativas) {
            this.alternativas.add(item);
        }
    }
    public void calcularPuntaje(Alternativa ... alternativas){

    }

    public Integer getPuntajeCalculado() {
        return puntajeCalculado;
    }
}
