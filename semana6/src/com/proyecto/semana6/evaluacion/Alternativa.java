package com.proyecto.semana6.evaluacion;

public class Alternativa {
    private String enunciado;
    private boolean correcto;
    public Alternativa(String enunciado, boolean correcto) {
        this.enunciado = enunciado;
        this.correcto = correcto;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public boolean isCorrecto() {
        return correcto;
    }
}
