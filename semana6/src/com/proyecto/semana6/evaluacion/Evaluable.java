package com.proyecto.semana6.evaluacion;

public interface Evaluable {
    void calcularPuntaje();
    public void agregarPreguntas(Calculable ... preguntas);
}
