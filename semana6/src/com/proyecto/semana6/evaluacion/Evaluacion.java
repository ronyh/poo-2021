package com.proyecto.semana6.evaluacion;

import java.util.ArrayList;
import java.util.List;

public class Evaluacion implements Evaluable{
    private String nombre;
    private List<Calculable> preguntas;
    private Integer puntajeTotal;
    private Integer puntajeCalculado;

    public Evaluacion(String nombre, Integer puntajeTotal) {
        this.nombre = nombre;
        this.puntajeTotal = puntajeTotal;
        this.preguntas = new ArrayList<>();
        this.puntajeCalculado = 0;
    }
    public void agregarPreguntas(Calculable ... preguntas){
        for (Calculable pregunta: preguntas) {
            this.preguntas.add(pregunta);
        }
    }
    public void calcularPuntaje(){
        for (Calculable pregunta: preguntas) {
            pregunta.calcularPuntaje();
            this.puntajeCalculado  += pregunta.getPuntajeCalculado();
        }
    }
}
