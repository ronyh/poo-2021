package com.proyecto.semana6.colecciones;

import com.proyecto.semana6.excepciones.ExcepcionNegativo;

import java.util.*;

public class Aplicacion {


    public static void main(String[] args) {
        MapaEnteros mapaEnteros = new MapaEnteros();
        System.out.println("Inicio del proceso");
        try{
            mapaEnteros.llenarLista(10,-10,100,20,20);
            mapaEnteros.llenarLista(20,50,50,200,-200);
        }catch (ExcepcionNegativo e){
            e.printStackTrace();
        }
        finally {
            mapaEnteros.procesarMapa();
        }
        System.out.println("Fin del proceso");
        /*
        for ( Integer item: listaSegunda) {
            if( mapa.containsKey(item)){
                mapa.replace(item, mapa.get(item) + 1);
            }
            else{
                mapa.put(item, 1);
            }

        }*/

        /*
        for (Integer llave: mapa.keySet()) {
            System.out.println(llave);
        }
        for (Integer valor: mapa.values()) {
            System.out.println(valor);
        }*/


    }

}
