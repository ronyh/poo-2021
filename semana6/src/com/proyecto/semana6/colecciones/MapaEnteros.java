package com.proyecto.semana6.colecciones;

import com.proyecto.semana6.excepciones.ExcepcionNegativo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapaEnteros {
    List<Integer> lista;
    Map<Integer,Integer> mapa;

    public MapaEnteros() {
        this.lista = new ArrayList<Integer>();
        this.mapa = new HashMap<>();
    }
    public void procesarMapa(){
        llenarMapa();
        mostrarDatosMapa();
    }
    private void mostrarDatosMapa(){
        for (Integer llave: mapa.keySet()) {
            System.out.println("Key: "+ llave+ " Value: "+ mapa.get(llave));
        }
    }
    public void llenarLista(Integer ... valores) throws ExcepcionNegativo {
        for ( Integer item: valores) {
            if(item < 0){
                throw new ExcepcionNegativo();
            }
            lista.add(item);
        }
    }
    private void llenarMapa(){
        for ( Integer item: lista) {
            if( mapa.containsKey(item)){
                mapa.replace(item, mapa.get(item) + 1);
            }
            else{
                mapa.put(item, 1);
            }
        }
    }
}
