package pe.edu.uni.fiis.repaso1.controlador;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pe.edu.uni.fiis.repaso1.dto.Persona;
import pe.edu.uni.fiis.repaso1.servicio.Respiracion;
import pe.edu.uni.fiis.repaso1.servicio.RespiracionTipoA;
import pe.edu.uni.fiis.repaso1.servicio.RespiracionTipoAB;

import java.io.FileOutputStream;
import java.io.OutputStream;

public class Aplicacion {
    public static void main(String[] args) {
        Persona angelo = new Persona("Angelo","A");
        Persona rony = new Persona("Rony","AB");

        Respiracion respiracion = null;
        procesar(respiracion, angelo);
        procesar(respiracion, angelo);
        procesar(respiracion, rony);
        procesar(respiracion, rony);
        crearReporte(angelo, rony);

    }
    private static void procesar(Respiracion respiracion, Persona persona){
        if(persona.getTipoSangre().equals("A")){
            respiracion = new RespiracionTipoA();
        }
        else if(persona.getTipoSangre().equals("AB")){
            respiracion = new RespiracionTipoAB();
        }
        respiracion.respirar(persona);
    }
    private static void crearReporte(Persona ... personas){
        Workbook wb = new XSSFWorkbook();

        try {
            OutputStream fileOut = new FileOutputStream("D:\\temp\\libro.xlsx");
            Sheet hoja1 = wb.createSheet("Nueva hoja");
            Integer numeroFila = 1;

            Row rowTitulo = hoja1.createRow(0);
            Cell celdaNombreTitulo = rowTitulo.createCell(0);
            celdaNombreTitulo.setCellValue("Nombre");

            Cell celdaTipoSangreTitulo = rowTitulo.createCell(1);
            celdaTipoSangreTitulo.setCellValue("Tipo de Sangre");

            Cell celdaOxigenoTitulo = rowTitulo.createCell(2);
            celdaOxigenoTitulo.setCellValue("Cantidad de oxígeno");

            for (Persona persona: personas) {
                Row row = hoja1.createRow(numeroFila);
                Cell celdaNombre = row.createCell(0);
                celdaNombre.setCellValue(persona.getNombre());

                Cell celdaTipoSangre = row.createCell(1);
                celdaTipoSangre.setCellValue(persona.getTipoSangre());

                Cell celdaOxigeno = row.createCell(2);
                celdaOxigeno.setCellValue(persona.getCantidadOxigeno());
                numeroFila++;
            }


            wb.write(fileOut);
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }
}
