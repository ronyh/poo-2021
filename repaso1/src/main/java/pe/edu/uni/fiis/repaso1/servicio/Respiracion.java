package pe.edu.uni.fiis.repaso1.servicio;

import pe.edu.uni.fiis.repaso1.dto.Persona;

public abstract class Respiracion {

    public void respirar(Persona persona){
        persona.setCantidadOxigeno(persona.getCantidadOxigeno()+100);
        System.out.println(persona.getCantidadOxigeno());
    }
}
