package pe.edu.uni.fiis.introduccion.primerapractica.pregunta1;

public class Aplicacion {
    public static void main(String[] args) {
        Calzado deportivo = new Calzado();
        deportivo.setHorma("Delgada");
        Cliente rony = new Cliente();
        rony.setDni("5050500");
        Venta venta = new Venta();
        venta.setCalzado(deportivo);
        venta.setCliente(rony);
    }
}
