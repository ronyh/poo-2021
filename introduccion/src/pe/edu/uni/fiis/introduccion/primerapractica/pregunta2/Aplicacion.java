package pe.edu.uni.fiis.introduccion.primerapractica.pregunta2;

public class Aplicacion {
    public static void main(String[] args) {
        PoligonoRegular cuadrado = new PoligonoRegular(2.0,4);
        cuadrado.calcularArea();
        cuadrado.calcularPerimetro();
        cuadrado.calcularDiagonales();
        System.out.println(cuadrado.getArea());
        System.out.println(cuadrado.getPerimetro());
        System.out.println(cuadrado.getDiagonales());
    }
}
