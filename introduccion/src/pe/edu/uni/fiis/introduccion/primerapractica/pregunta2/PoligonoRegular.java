package pe.edu.uni.fiis.introduccion.primerapractica.pregunta2;

public class PoligonoRegular {
    private double area;
    private double perimetro;
    private double diagonales;
    private double longitudLado;
    private double cantidadLados;

    public PoligonoRegular(double longitudLado, double cantidadLados) {
        this.longitudLado = longitudLado;
        this.cantidadLados = cantidadLados;
    }

    public void calcularArea(){
        calcularPerimetro();
        this.area = this.obtenerApotema()*this.perimetro/2;
    }
    public double obtenerApotema(){
        return 1;
    }
    public void calcularPerimetro(){
        this.perimetro = this.longitudLado*this.cantidadLados;
    }
    public void calcularDiagonales(){
        this.diagonales = this.cantidadLados*((this.cantidadLados-3)/2);
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    public double getDiagonales() {
        return diagonales;
    }

    public void setDiagonales(double diagonales) {
        this.diagonales = diagonales;
    }

    public double getLongitudLado() {
        return longitudLado;
    }

    public void setLongitudLado(double longitudLado) {
        this.longitudLado = longitudLado;
    }

    public double getCantidadLados() {
        return cantidadLados;
    }

    public void setCantidadLados(double cantidadLados) {
        this.cantidadLados = cantidadLados;
    }
}
