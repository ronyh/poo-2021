package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta1;

import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        Ecuacion ecuacion = new Ecuacion();
        System.out.println("Ingrese a: ");
        ecuacion.setPrimerCoeficiente(entrada.nextDouble());
        System.out.println("Ingrese b: ");
        ecuacion.setSegundoCoeficiente(entrada.nextDouble());
        System.out.println("Ingrese c: ");
        ecuacion.setTercerCoeficiente(entrada.nextDouble());
        ecuacion.calcularRaices();
    }
}
