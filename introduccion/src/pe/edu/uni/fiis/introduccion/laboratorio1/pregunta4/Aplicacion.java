package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta4;

import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int[] lista = new int[15];// new int[]{1,2,3,4,5,6,4,8,8,4,45}
        int indice = 0;
        do{
            System.out.println("Ingrese número "+(indice+1)+": ");
            lista[indice] = entrada.nextInt();
            //indice = indice + 1;
            indice++;
        }while(indice<15);
        Conjunto conjunto = new Conjunto(lista);
        conjunto.sumar();
        System.out.println("La suma es: " + conjunto.getSuma());
    }
}
