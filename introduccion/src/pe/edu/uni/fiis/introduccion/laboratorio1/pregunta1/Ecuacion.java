package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta1;

public class Ecuacion {
    private double primerCoeficiente=0;
    private double segundoCoeficiente=0;
    private double tercerCoeficiente=0;
    private double raiz1=0;
    private double raiz2=0;
    private double discriminante=0;
    public Ecuacion(){
    }
    public double getPrimerCoeficiente() {
        return primerCoeficiente;
    }
    public void calcularRaices(){
        this.calcularDiscriminante();
        if(this.discriminante >= 0){
            raiz1 = (-this.segundoCoeficiente +
                    Math.pow( discriminante,0.5))/(2*this.primerCoeficiente);
            raiz2 = (-this.segundoCoeficiente -
                    Math.pow( discriminante,0.5))/(2*this.primerCoeficiente);
            System.out.println("Raiz 1: "+raiz1);
            System.out.println("Raiz 2: "+raiz2);
        }
        else{
            System.out.println("Las raices no existen");
        }
    }
    private void calcularDiscriminante(){
        this.discriminante = Math.pow(this.segundoCoeficiente,2) -
                                4 *this.primerCoeficiente*this.tercerCoeficiente ;
    }

    public void setPrimerCoeficiente(double primerCoeficiente) {
        this.primerCoeficiente = primerCoeficiente;
    }

    public double getSegundoCoeficiente() {
        return segundoCoeficiente;
    }

    public void setSegundoCoeficiente(double segundoCoeficiente) {
        this.segundoCoeficiente = segundoCoeficiente;
    }

    public double getTercerCoeficiente() {
        return tercerCoeficiente;
    }

    public void setTercerCoeficiente(double tercerCoeficiente) {
        this.tercerCoeficiente = tercerCoeficiente;
    }

    public double getRaiz1() {
        return raiz1;
    }

    public void setRaiz1(double raiz1) {
        this.raiz1 = raiz1;
    }

    public double getRaiz2() {
        return raiz2;
    }

    public void setRaiz2(double raiz2) {
        this.raiz2 = raiz2;
    }
}
