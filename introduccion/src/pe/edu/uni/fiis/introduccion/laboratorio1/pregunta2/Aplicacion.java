package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta2;

import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Ingrese el radio del circulo:");
        Circulo circulo = new Circulo(entrada.nextDouble());
        System.out.println("Radio: "+ circulo.getRadio());
        System.out.println("Area: "+ circulo.getArea());
    }
}
