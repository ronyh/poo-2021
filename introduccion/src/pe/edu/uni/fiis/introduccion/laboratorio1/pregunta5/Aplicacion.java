package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta5;

import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        Factura[] lista = new Factura[5];
        for (int i = 0; i < lista.length; i++) {
            System.out.println("Ingrese código de artículo");
            //entrada.nextLine();
            String codigoArticulo = entrada.next();
            System.out.println("Ingrese cantidad de litros");
            int litros = entrada.nextInt();
            System.out.println("Ingrese precio por litro");
            double precioLitro = entrada.nextDouble();
            lista[i] = new Factura(codigoArticulo,litros,precioLitro);
        }
        Factura.calcularTotal(lista);
        Factura.calcularLitros(lista,"1");
        Factura.calcularFacturaMayores(lista,600);

    }
}
