package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta2;

public class Circulo {
    private double radio;
    private double area;

    public Circulo(double radio){
        this.radio = radio;
        calcularArea();
    }

    public void calcularArea(){
        this.area = Math.PI * Math.pow(radio, 2);
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }
}
