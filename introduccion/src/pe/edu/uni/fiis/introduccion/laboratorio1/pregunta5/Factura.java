package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta5;

public class Factura {
    private String codigoArticulo;
    private int litros;
    private double precioLitro;
    private double total;

    public Factura(String codigoArticulo, int litros, double precioLitro) {
        this.codigoArticulo = codigoArticulo;
        this.litros = litros;
        this.precioLitro = precioLitro;
        this.total = this.precioLitro * this.litros;
    }
    public static void calcularTotal(Factura[] lista){
        double suma = 0;
        for (int i = 0; i < lista.length; i++) {
            suma += lista[i].total;
        }
        System.out.println("Facturación total: "+ suma);
    }
    public static double calcularLitros(Factura[] lista, String codigoArticulo){
        double litros = 0;
        for (int i = 0; i < lista.length; i++) {
            if(codigoArticulo.equals(lista[i].codigoArticulo)){
                litros += lista[i].litros;
            }
        }
        System.out.println("Total de litros: "+ litros);
        return litros;
    }
    public static int calcularFacturaMayores(Factura[] lista, double total){
        int cantidad = 0;
        for (int i = 0; i < lista.length; i++) {
            if(total <= lista[i].total){
                cantidad++;
            }
        }
        System.out.println("Cantidad de facturas mayores a "+total+": "+ cantidad);
        return cantidad;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public int getLitros() {
        return litros;
    }

    public void setLitros(int litros) {
        this.litros = litros;
    }

    public double getPrecioLitro() {
        return precioLitro;
    }

    public void setPrecioLitro(double precioLitro) {
        this.precioLitro = precioLitro;
    }
}
