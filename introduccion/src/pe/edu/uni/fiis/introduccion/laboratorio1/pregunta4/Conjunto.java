package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta4;

public class Conjunto {
    private int arreglo[];
    private int suma;

    public Conjunto(int[] arreglo) {
        this.arreglo = arreglo;
    }

    public void sumar(){
        int indice = 0;
        this.suma = 0;
        while( indice < arreglo.length){
            //this.suma = this.suma + arreglo[indice];
            this.suma += arreglo[indice];
            indice++;
        }
    }

    public int[] getArreglo() {
        return arreglo;
    }

    public void setArreglo(int[] arreglo) {
        this.arreglo = arreglo;
    }

    public int getSuma() {
        return suma;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }
}
