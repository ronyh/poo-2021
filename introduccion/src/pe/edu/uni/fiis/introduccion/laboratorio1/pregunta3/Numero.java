package pe.edu.uni.fiis.introduccion.laboratorio1.pregunta3;

public class Numero {
    private int inicio;
    private int fin;
    private int incremento;

    public Numero(int inicio, int fin, int incremento) {
        this.inicio = inicio;
        this.fin = fin;
        this.incremento = incremento;
    }
    public void generarSerie(){
        //for (inicialización; condiciones; pasos finales)
        for (int i = this.inicio; i >= this.fin; i = i + incremento) {
            System.out.println(i);
        }
    }
}
