package pe.edu.uni.fiis.introduccion.sesion4;

public class Curso {
    private int nota1;
    private int nota2;
    private int nota3;

    public Curso(int nota1, int nota2, int nota3) {
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
    }

    public Curso(int nota1, int nota2) {
        this(nota1,nota2,0);
    }

    public void asignar() {
        asignar(8);
    }

    public void asignar(int nota1) {
        asignar(nota1,0);
    }

    public void asignar(int nota1, int nota2) {
        asignar(nota1,nota2,0);
    }

    public void asignar(int nota1, int nota2, int nota3) {
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
    }
}
