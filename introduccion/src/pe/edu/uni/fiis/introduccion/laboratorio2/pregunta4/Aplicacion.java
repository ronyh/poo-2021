package pe.edu.uni.fiis.introduccion.laboratorio2.pregunta4;

public class Aplicacion {
    String data;
    class Rony{//inner class
        private String nombre;

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
    }
    public static void main(String[] args) {
        Aplicacion aplicacion = new Aplicacion();
        Rony rony = aplicacion.new Rony();
        Rony rony1 = new Aplicacion().new Rony();
        rony.getNombre();

        /*
        Serie serie = new Serie();
        serie.asignarArreglo(10);
        System.out.println(serie.mediaPositivos());
        System.out.println(serie.mediaNegativos());
        System.out.println(serie.obtenerCeros());
        */
        System.out.println(sumar(1));
        System.out.println(sumar(1,2));
        System.out.println(sumar(1,2,3));
        System.out.println(sumar(1,2,3,4));
        System.out.println(sumar(1,2,3,4,5));
        int[] i = {2,43,5,7};
        System.out.println(sumar(i));
        System.out.println(sumar(1));
    }
    public static int sumar(int i){
        int suma = 0;
        suma += (i*5);
        return suma;
    }
    public static int sumar(int ... arreglo){
        int suma = 0;
        for (int elemento: arreglo) {
            suma += elemento;
        }
        return suma;
    }
}
