package pe.edu.uni.fiis.introduccion.laboratorio2.pregunta3;

public class Aplicacion {
    public static void main(String[] args) {
        Serie serie = new Serie();
        serie.generarImpares(10);
        System.out.println(serie.multiplicar());
        serie.dato = 34;

        Serie serie2 = new Serie();

        serie2.dato = 40;
        System.out.println(serie.dato);
        System.out.println(serie2.dato);
        System.out.println(Serie.dato);
    }
}
