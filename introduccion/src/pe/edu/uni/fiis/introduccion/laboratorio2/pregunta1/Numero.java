package pe.edu.uni.fiis.introduccion.laboratorio2.pregunta1;

public class Numero {
    public static void comparacion(int a, int b){
        imprimir( a == b);
    }
    public static void imprimir(boolean condicion){
        //if a == b :
        //    sentencias
        //elif condicion:
        //    sentencias
        //else:
        //    sentencias
        if( condicion){
            System.out.println("Son iguales");
        }
        /*
        else if(a<b){
        }
         */
        else{
            System.out.println("No son iguales");
        }
    }
}
