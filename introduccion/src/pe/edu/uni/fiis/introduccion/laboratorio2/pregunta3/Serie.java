package pe.edu.uni.fiis.introduccion.laboratorio2.pregunta3;

public class Serie {
    private int[] numeros;
    public static int dato;

    // Diseñar un programa que muestre el producto de los 10 primeros números impares
    public long multiplicar(){
        long resultado = 1;
        //for i in range(1,longitud,1):
        //  sentencias
        //for inicialización, condiciones, siguientes pasos
        for (int i = 0; i < this.numeros.length; i++) {
            //resultado = resultado * numeros[i];
            resultado *= this.numeros[i];
            //i = i + 1;
            //i++
        }
        return resultado;
    } 
    public void generarImpares(int limite){
        this.numeros = new int[limite];
        //while condiciones:
        // sentencias
        int indice = 0;
        while(indice < limite){
            this.numeros[indice] = 2*indice + 1;
            indice++;
        }
    }
}
