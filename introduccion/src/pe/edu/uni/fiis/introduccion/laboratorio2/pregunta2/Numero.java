package pe.edu.uni.fiis.introduccion.laboratorio2.pregunta2;

public class Numero {
    private int primer;
    private int segundo;

    public Numero(int primer, int segundo) {
        this.primer = primer;
        this.segundo = segundo;
    }
    public void mayor(){
        if(this.primer > this.segundo){
            System.out.println("El mayor es: "+ this.primer);
        }
        else if(this.primer == this.segundo){
            System.out.println("Son iguales");
        }
        else{
            System.out.println("El mayor es: "+ this.segundo);
        }
    }
}


