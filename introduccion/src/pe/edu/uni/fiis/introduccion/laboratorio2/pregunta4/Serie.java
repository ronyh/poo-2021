package pe.edu.uni.fiis.introduccion.laboratorio2.pregunta4;

import java.util.Scanner;

public class Serie {
    private int[] numeros;
    public void asignarArreglo(int longitud){
        Scanner entrada = new Scanner(System.in);
        int indice = 0;
        this.numeros = new int[longitud];
        do{
            System.out.println("Ingrese el número "+ (indice + 1) + ":");
            this.numeros[indice] = entrada.nextInt();
            indice++;
        }while (indice<longitud);
    }
    public double mediaPositivos(){
        int cantidad = 0;
        int suma = 0;
        for (int elemento: this.numeros){
            if(elemento > 0){
                suma += elemento;
                cantidad++;
            }
        }
        if(cantidad == 0){
            return 0;
        }
        return suma/cantidad;
    }

    public double mediaNegativos(){
        int cantidad = 0;
        int suma = 0;
        for (int i = 0; i < this.numeros.length; i++) {
            if(this.numeros[i] < 0){
                suma += this.numeros[i];
                cantidad++;
            }
        }
        if(cantidad == 0){
            return 0;
        }
        return suma/cantidad;
    }
    public int obtenerCeros(){
        int cantidad = 0;
        int indice = 0;
        while(indice < this.numeros.length){
            if(this.numeros[indice] == 0) cantidad++;
            /*
            if(this.numeros[indice] == 0){
                cantidad++;
            }
            */
            indice++;
        }
        return cantidad;
    }
}
