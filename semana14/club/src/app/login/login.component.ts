import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  usuario: string = "";
  clave: string = "";
  constructor() { }

  ngOnInit(): void {
  }
  ingresar(): void {
    console.log('Usuario: ' + this.usuario);
    console.log('Clave: ' + this.clave);
  }

}
