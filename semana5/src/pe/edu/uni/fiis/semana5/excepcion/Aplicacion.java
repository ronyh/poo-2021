package pe.edu.uni.fiis.semana5.excepcion;

public class Aplicacion {
    public static void main(String[] args) {
        int a = 0;
        int b = 2;
        try {
            mostrar(a,b);
        } catch (MiException e) {
            e.printStackTrace();
        }
        finally {
            System.out.println("final");
        }
        System.out.println("final del programa");
    }
    public static void mostrar(int a, int b) throws MiException{
        if(a<b){
            throw new MiException("a no debe ser menor a b");
        }
        System.out.println(b);
    }
}
