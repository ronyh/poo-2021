package pe.edu.uni.fiis.semana5.excepcion;

public class MiException extends Exception{
    public MiException(String message) {
        super(message);
    }
}
