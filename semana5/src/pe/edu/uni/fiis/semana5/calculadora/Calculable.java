package pe.edu.uni.fiis.semana5.calculadora;

public interface Calculable {
    public abstract double mover(int pasos);
    public abstract double restar(int primero, int segundo, Signo signo);
}
