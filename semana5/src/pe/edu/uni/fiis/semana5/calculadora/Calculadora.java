package pe.edu.uni.fiis.semana5.calculadora;

import pe.edu.uni.fiis.semana5.singleton.SingletonVehiculo;
import pe.edu.uni.fiis.semana5.singleton.Vehiculo;

public class Calculadora implements Calculable, Contable{
    public static void main(String[] args) {
        Calculadora calculadora = new Calculadora();
        calculadora.restar(12,15,Signo.RESTA);

    }

    public double mover(int pasos) {
        Vehiculo vehiculo = SingletonVehiculo.getInstancia();
        vehiculo.avanzar(pasos);
        return vehiculo.getPosicion();
    }

    public double restar(int primero, int segundo, Signo signo) {
        System.out.println(signo.getSimbolo());
        return primero - segundo;
    }

    public void contar(int limiteInferior, int limiteSuperior) {
        for (int i = limiteInferior; i < limiteSuperior; i++) {
            System.out.println(i);

        }
    }
}
