package pe.edu.uni.fiis.semana5.calculadora;

public enum Signo {
    SUMA("suma","+"), RESTA("resta","-"),
    PRODUCTO("multiplicación",".");
    private String operacion;
    private String simbolo;

    Signo(String operacion, String simbolo) {
        this.operacion = operacion;
        this.simbolo = simbolo;
    }

    public String getOperacion() {
        return operacion;
    }

    public String getSimbolo() {
        return simbolo;
    }
}
