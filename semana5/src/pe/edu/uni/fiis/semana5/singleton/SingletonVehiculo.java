package pe.edu.uni.fiis.semana5.singleton;

public abstract class SingletonVehiculo {
    private static Vehiculo vehiculo;

    public static Vehiculo getInstancia() {
        if( vehiculo == null){
            vehiculo = new Vehiculo("A2",15.0,15.5);
        }
        return vehiculo;
    }
}
