package pe.edu.uni.fiis.semana5.polimorfismo.figura;

public abstract class Figura {
    private String nombre;
    public abstract double calcularArea();
    public abstract double calcularPerimetro();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
