package pe.edu.uni.fiis.semana5.polimorfismo;

public class Aplicacion {
    public static void main(String[] args) {
        RepositorioBaseDatos repositorioBaseDatos = new RepositorioBaseDatos();
        Repositorio repositorio = repositorioBaseDatos;
        Contable contable = repositorioBaseDatos;
        System.out.println(repositorio.obtenerRegistro("2"));
    }
}
