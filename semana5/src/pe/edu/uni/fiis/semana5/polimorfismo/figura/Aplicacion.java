package pe.edu.uni.fiis.semana5.polimorfismo.figura;

public class Aplicacion {
    public static void main(String[] args) {
        Circulo circulo = new Circulo();
        circulo.setRadio(34);
        Cuadrado cuadrado = new Cuadrado();
        cuadrado.setLado(32);
        System.out.println(sumarAreaYPerimetro(circulo));
        System.out.println(sumarAreaYPerimetro(cuadrado));

        Circulo circuloNuevo = new CirculoNuevo();
        circuloNuevo.setRadio(34);

        mostrarMensajes(circulo);
        mostrarMensajes(circuloNuevo);


    }
    public static double sumarAreaYPerimetro(Figura figura){
        return figura.calcularArea()+figura.calcularPerimetro();
    }
    public static void mostrarMensajes(Circulo circulo){
        System.out.println(circulo.calcularArea());
        System.out.println(circulo.calcularPerimetro());
    }
}
