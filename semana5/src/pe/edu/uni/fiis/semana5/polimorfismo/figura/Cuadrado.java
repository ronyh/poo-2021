package pe.edu.uni.fiis.semana5.polimorfismo.figura;

public class Cuadrado extends Figura/*implements Figurable*/{
    private double lado;

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    public double calcularArea() {
        return Math.pow(this.lado,2);
    }

    public double calcularPerimetro() {
        return 4 * this.lado;
    }
}
