package pe.edu.uni.fiis.semana5.polimorfismo.figura;

public class CirculoNuevo extends Circulo{
    public double calcularArea() {
        return Math.PI*Math.pow(this.getRadio(),2)+1;
    }
}
