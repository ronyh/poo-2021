package pe.edu.uni.fiis.semana5.polimorfismo.figura;

public interface Figurable {
    double calcularArea();
    double calcularPerimetro();
}
