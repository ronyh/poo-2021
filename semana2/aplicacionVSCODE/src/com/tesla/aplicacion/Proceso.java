package com.tesla.aplicacion;

public class Proceso {
    private Venta[] ventas;
    public static void main(String[] args) {
        Proceso p = new Proceso();
        p.ventas = new Venta[2];

        Producto[] productos1 = new Producto[2];
        productos1[0] = new Producto();
        productos1[0].setCodigo(1);
        productos1[0].setNombre("Toalla");
        productos1[0].setPrecioUnitario(10.5);
        
        Producto pro2 = new Producto();
        pro2.setCodigo(2);
        pro2.setNombre("Jabon");
        pro2.setPrecioUnitario(5.5);
        productos1[1] = pro2;

        p.ventas[0] = new Venta();
        p.ventas[0].setProductos(productos1);

        Producto[] productos2 = new Producto[2];
        productos2[0] = new Producto();
        productos2[0].setCodigo(1);
        productos2[0].setNombre("Toalla");
        productos2[0].setPrecioUnitario(10.5);
        
        pro2 = new Producto();
        pro2.setCodigo(2);
        pro2.setNombre("Jabon");
        pro2.setPrecioUnitario(5.5);
        productos2[1] = pro2;

        p.ventas[1] = new Venta();
        p.ventas[1].setProductos(productos2);
        System.out.println(p.calcularTotal());
    }
    public Double calcularTotal(){
        Double total = 0D;
        for (int i = 0; i < this.ventas.length; i++) {
            for (int j = 0; j < this.ventas[i].getProductos().length; j++) {
                total = total + this.ventas[i].getProductos()[j].getPrecioUnitario(); 
            }
        }
        return total;
    }
}
