package pe.edu.fiis.poo.laboratorio2.pregunta1;

import java.util.Scanner;

public class EcuacionSegundoGrado {
    private Integer coeficiente1;
    private Integer coeficiente2;
    private Integer coeficiente3;
    private Double primeraRaiz;
    private Double segundaRaiz;

    public Double sumarRaices(){
        //return getPrimeraRaiz()+ getSegundaRaiz();
        return primeraRaiz+ segundaRaiz;
    }

    public static void main(String[] args) {
        EcuacionSegundoGrado solucion = new EcuacionSegundoGrado();
        System.out.println("Indique las coeficientes de una ecuacion: ");
        Scanner entrada= new Scanner(System.in);
        solucion.setCoeficiente1(entrada.nextInt());
        solucion.setCoeficiente2(entrada.nextInt());
        solucion.setCoeficiente3(entrada.nextInt());
        solucion.calcularRaices();
    }
    public void calcularRaices(){
        primeraRaiz = (coeficiente2+Math.pow((Math.pow(coeficiente2, 2)-4*coeficiente1*coeficiente3), 0.5))/2;
        segundaRaiz = (coeficiente2-Math.pow((Math.pow(coeficiente2, 2)-4*coeficiente1*coeficiente3), 0.5))/2;
        System.out.println("La primera raiz es "+primeraRaiz);
        System.out.println("La segunda raiz es "+segundaRaiz);
    }

    public Double getPrimeraRaiz() {
        return primeraRaiz;
    }

    public void setPrimeraRaiz(Double primeraRaiz) {
        this.primeraRaiz = primeraRaiz;
    }

    public Double getSegundaRaiz() {
        return segundaRaiz;
    }

    public void setSegundaRaiz(Double segundaRaiz) {
        this.segundaRaiz = segundaRaiz;
    }

    public Integer getCoeficiente1() {
        return coeficiente1;
    }

    public void setCoeficiente1(Integer coeficiente1) {
        this.coeficiente1 = coeficiente1;
    }

    public Integer getCoeficiente2() {
        return coeficiente2;
    }

    public void setCoeficiente2(Integer coeficiente2) {
        this.coeficiente2 = coeficiente2;
    }

    public Integer getCoeficiente3() {
        return coeficiente3;
    }

    public void setCoeficiente3(Integer coeficiente3) {
        this.coeficiente3 = coeficiente3;
    }
}
