package com.tesla.aplicacion;

public class Venta {
    private Producto[] productos;

    public Producto[] getProductos() {
        return productos;
    }

    public void setProductos(Producto[] productos) {
        this.productos = productos;
    }
}
