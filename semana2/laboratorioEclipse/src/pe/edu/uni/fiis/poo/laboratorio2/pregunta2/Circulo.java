package pe.edu.uni.fiis.poo.laboratorio2.pregunta2;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Scanner;

public class Circulo {
	private Double radio;
	public Circulo() {
		this.radio = 0d;
	}
	public Circulo(Double radio) {
		this.radio = radio;
	}
	public Double getRadio() {
		return radio;
	}

	public void setRadio(Double radio) {
		this.radio = radio;
	}
	
	public Double calcularArea() {
		return Math.PI*Math.pow(radio, 2);
	}

	public static void main(String[] args) {
		/*Scanner entrada= new Scanner(System.in);
		System.out.println("Ingrese radio: ");
		Circulo circulo = new Circulo();
		circulo.setRadio(entrada.nextDouble());
		System.out.println(circulo.calcularArea());
		System.out.println(Figura.calcularAreaCirculo(circulo.getRadio()));
		System.out.println(Figura.calcularAreaCirculo(circulo));*/
		String auth = "integraciones.visanet@necomplus.com" + ":" + "d5e7nk$M";
        byte[] encodedAuth = Base64.getEncoder().encode(
                auth.getBytes(StandardCharsets.ISO_8859_1));
        String authHeader = "Basic " + new String(encodedAuth);
        System.out.println(authHeader);
	}

}
