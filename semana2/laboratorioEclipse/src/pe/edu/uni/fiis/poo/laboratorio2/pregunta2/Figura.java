package pe.edu.uni.fiis.poo.laboratorio2.pregunta2;

public class Figura {
	
	public static Double calcularAreaCirculo(Double radio) {
		return Math.PI*Math.pow(radio, 2);
	}
	public static Double calcularAreaCirculo(Circulo circulo) {
		return Math.PI*Math.pow(circulo.getRadio(), 2);
	}
}
