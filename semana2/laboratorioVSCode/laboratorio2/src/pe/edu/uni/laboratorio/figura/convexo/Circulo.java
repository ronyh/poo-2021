package pe.edu.uni.laboratorio.figura.convexo;

public class Circulo {
    private Double radio;

    public Double getRadio() {
        return radio;
    }

    public void setRadio(Double radio) {
        this.radio = radio;
    }
    public Double calcularArea(){
        return Math.PI* Math.pow(this.radio, 2);
    }

    public Double calcularPerimetro(){
        return 2*this.radio*Math.PI;
    }

}
