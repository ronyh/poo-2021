package pe.edu.uni.laboratorio.figura.poligono;

public class Cuadrado {
    private Double lado;

    public Double getLado() {
        return lado;
    }

    public void setLado(Double lado) {
        this.lado = lado;
    }
    public Double calcularArea(){
        return Math.pow(this.lado, 2);
    }

    public Double calcularPerimetro(){
        return 4*this.lado;
    }
}
