package pe.edu.uni.laboratorio.figura;

import pe.edu.uni.laboratorio.figura.convexo.Circulo;
import pe.edu.uni.laboratorio.figura.poligono.Cuadrado;

public class Aplicacion {
    private Cuadrado cuadrado;
    public static void main(String[] args) {
        Circulo circulo = new Circulo();
        circulo.setRadio(5d);
        Cuadrado cuadrado = new Cuadrado();
        cuadrado.setLado(10d);
        System.out.println(circulo.calcularArea());
        System.out.println(circulo.calcularPerimetro());
        System.out.println(cuadrado.calcularArea());
        System.out.println(cuadrado.calcularPerimetro());
    }
    public Cuadrado getCuadrado() {
        return cuadrado;
    }
    public void setCuadrado(Cuadrado cuadrado) {
        this.cuadrado = cuadrado;
    }
}
