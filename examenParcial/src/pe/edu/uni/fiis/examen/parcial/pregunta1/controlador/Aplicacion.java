package pe.edu.uni.fiis.examen.parcial.pregunta1.controlador;

import pe.edu.uni.fiis.examen.parcial.pregunta1.dto.Autor;
import pe.edu.uni.fiis.examen.parcial.pregunta1.dto.Libro;
import pe.edu.uni.fiis.examen.parcial.pregunta1.servicio.Servicio;
import pe.edu.uni.fiis.examen.parcial.pregunta1.servicio.ServicioImpl;

import java.util.ArrayList;
import java.util.List;

public class Aplicacion {
    public static void main(String[] args) {
        Libro libro1 = new Libro();
        libro1.setTitulo("El quijote");
        libro1.setAutores(new ArrayList<>());
        Autor autor = new Autor();
        autor.setNombres("Cervantes");
        libro1.getAutores().add(autor);
        List<Libro> lista = new ArrayList<>();
        lista.add(libro1);
        Servicio servicio = new ServicioImpl(lista);
        List<Libro> listaEncontrados = servicio.buscarLibro(null,null,"Cervantes");
        for (Libro libro: listaEncontrados) {
            System.out.println(libro.getTitulo());
        }
    }
}
