package pe.edu.uni.fiis.examen.parcial.pregunta1.dto;

public class Autor {
    private String nombres;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
}
