package pe.edu.uni.fiis.examen.parcial.pregunta2.servicio;

import pe.edu.uni.fiis.examen.parcial.pregunta2.dto.Postulante;
import pe.edu.uni.fiis.examen.parcial.pregunta2.excepciones.NotaExcepcion;

public interface Servicio {
    void registrarPostulante(Postulante postulante);
    void asignarNota(Integer numeroIdentidad, Integer nota) throws NotaExcepcion;
}
