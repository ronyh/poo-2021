package pe.edu.uni.fiis.examen.parcial.pregunta3.servicio;

import pe.edu.uni.fiis.examen.parcial.pregunta3.dto.Venta;

import java.util.*;

public class ServicioImpl implements Servicio{
    private Set<Venta> relojesVendidos = new HashSet<>();
    private Map<String, Venta> mapasVentasTop = new HashMap<>();
    public void registrarVenta(Venta venta) {
        relojesVendidos.add(venta);

        if( mapasVentasTop.containsKey(venta.getProducto()) ){
            mapasVentasTop.get(venta.getProducto())
                    .setPrecioTotal(
                            mapasVentasTop.get(venta.getProducto()).getPrecioTotal() + venta.getPrecioTotal()
                    );
        }
        else{
            mapasVentasTop.put(venta.getProducto(), venta);
        }
    }

    public void mostrarReporteRelojesVendidos() {
        System.out.println("Reporte de relojes vendidos");
        for (Venta venta: relojesVendidos) {
            System.out.println("Reloj: " + venta.getProducto());
        }
    }

    public void mostrarRankingtop3() {
        System.out.println("Reporte de ranking top 3 por volumen de venta totales");
        List<Venta> ventas = convertirMapAListaTop3(mapasVentasTop);
        for (int i = 0; i < 3; i++) {
            System.out.println("Reloj: " + ventas.get(i).getProducto());
        }
    }
    private List<Venta> convertirMapAListaTop3(Map<String, Venta> mapa){
        List<Venta> listaTotal = new ArrayList<>(mapa.values());
        ordenarListaPorVentalTotal(listaTotal);
        return listaTotal;
    }
    void ordenarListaPorVentalTotal(List<Venta> lista){
        /**
         * Se puede usar compareTo también
         */
        for (int i = 0; i < lista.size()-1; i++) {
            for (int j = i; j < lista.size(); j++) {
                if( lista.get(i).getPrecioTotal() < lista.get(j).getPrecioTotal()){
                    Venta venta = lista.get(j);
                    lista.set(j,lista.get(i));
                    lista.set(i,venta);
                }
            }
        }
    }
}
