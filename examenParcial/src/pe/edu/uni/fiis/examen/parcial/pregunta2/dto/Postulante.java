package pe.edu.uni.fiis.examen.parcial.pregunta2.dto;

import pe.edu.uni.fiis.examen.parcial.pregunta2.enumeraciones.NivelEducativo;

public class Postulante {
    private Integer numeroIdentidad;
    private String nombres;
    private String apellidos;
    private String direccion;
    private String telefono;
    private Integer edad;
    private Integer notaEvaluacion;
    private NivelEducativo nivelEducativo;

    public Postulante(Integer numeroIdentidad, String nombres, String apellidos, String direccion, String telefono, Integer edad,NivelEducativo nivelEducativo) {
        this.numeroIdentidad = numeroIdentidad;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.edad = edad;
        this.notaEvaluacion = 0;
        this.nivelEducativo = nivelEducativo;
    }

    public Integer getNumeroIdentidad() {
        return numeroIdentidad;
    }

    public void setNumeroIdentidad(Integer numeroIdentidad) {
        this.numeroIdentidad = numeroIdentidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getNotaEvaluacion() {
        return notaEvaluacion;
    }

    public void setNotaEvaluacion(Integer notaEvaluacion) {
        this.notaEvaluacion = notaEvaluacion;
    }

    public NivelEducativo getNivelEducativo() {
        return nivelEducativo;
    }

    public void setNivelEducativo(NivelEducativo nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }
}
