package pe.edu.uni.fiis.examen.parcial.pregunta3.controlador;

import pe.edu.uni.fiis.examen.parcial.pregunta3.servicio.Servicio;
import pe.edu.uni.fiis.examen.parcial.pregunta3.servicio.ServicioImpl;
import pe.edu.uni.fiis.examen.parcial.pregunta3.dto.Venta;

public class Aplicacion {
    public static void main(String[] args) {
        Venta venta1 = new Venta("Seiko",10, 45200.0);
        Venta venta2 = new Venta("Fossil",1, 200.0);
        Venta venta3 = new Venta("Casio",20, 5200.0);
        Venta venta4 = new Venta("Seiko",2, 220.0);
        Venta venta5 = new Venta("Seiko",5, 500.0);
        Venta venta6 = new Venta("Rolex",1, 56200.0);

        Servicio servicio = new ServicioImpl();
        servicio.registrarVenta(venta1);
        servicio.registrarVenta(venta2);
        servicio.registrarVenta(venta3);
        servicio.registrarVenta(venta4);
        servicio.registrarVenta(venta5);
        servicio.registrarVenta(venta6);

        servicio.mostrarReporteRelojesVendidos();
        servicio.mostrarRankingtop3();
    }
}
