package pe.edu.uni.fiis.examen.parcial.pregunta1.dto;

public class Capitulo {
    private String titulo;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
