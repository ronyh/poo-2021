package pe.edu.uni.fiis.examen.parcial.pregunta1.servicio;

import pe.edu.uni.fiis.examen.parcial.pregunta1.dto.Libro;

import java.util.List;

public interface Servicio {
    public List<Libro> buscarLibro(String codigo, String titulo, String nombreAutor);
}
