package pe.edu.uni.fiis.examen.parcial.pregunta2.excepciones;

public class NotaExcepcion extends Exception{
    public NotaExcepcion(String message) {
        super(message);
    }
}
