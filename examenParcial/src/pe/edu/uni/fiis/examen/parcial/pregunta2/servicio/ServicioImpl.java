package pe.edu.uni.fiis.examen.parcial.pregunta2.servicio;

import pe.edu.uni.fiis.examen.parcial.pregunta2.dto.Postulante;
import pe.edu.uni.fiis.examen.parcial.pregunta2.enumeraciones.LimiteNota;
import pe.edu.uni.fiis.examen.parcial.pregunta2.excepciones.NotaExcepcion;

import java.util.HashMap;
import java.util.Map;

public class ServicioImpl implements Servicio{
    private Map<Integer, Postulante> mapaPostulante = new HashMap<>();

    public void registrarPostulante(Postulante postulante) {
        if( ! mapaPostulante.containsKey(postulante.getNumeroIdentidad())){
            mapaPostulante.put(postulante.getNumeroIdentidad(), postulante);
        }
    }
    public void asignarNota(Integer numeroIdentidad, Integer nota) throws NotaExcepcion {
        if(mapaPostulante.containsKey(numeroIdentidad)){
            if(validarNota(nota)){
                mapaPostulante.get(numeroIdentidad).setNotaEvaluacion(nota);
            }
        }
    }
    private Boolean validarNota(Integer nota) throws NotaExcepcion {
        Boolean valido = true;
        if(nota < LimiteNota.INFERIOR.getValor() || nota > LimiteNota.SUPERIOR.getValor()){
            valido = false;
            throw new NotaExcepcion("Nota fuera de rango posible");
        }
        return valido;
    }
}
