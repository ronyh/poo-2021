package pe.edu.uni.fiis.examen.parcial.pregunta1.servicio;

import pe.edu.uni.fiis.examen.parcial.pregunta1.dto.Autor;
import pe.edu.uni.fiis.examen.parcial.pregunta1.dto.Libro;

import java.util.ArrayList;
import java.util.List;

public class ServicioImpl implements Servicio{
    private List<Libro> listaLibros;

    public ServicioImpl(List<Libro> listaLibros) {
        this.listaLibros = listaLibros;
    }

    public void setListaLibros(List<Libro> listaLibros) {
        this.listaLibros = listaLibros;
    }

    public List<Libro> buscarLibro(String codigo, String titulo, String nombreAutor) {
        List<Libro> lista = new ArrayList<>();

        for (Libro libro: this.listaLibros) {
            if(codigo != null && libro.getCodigo().equals(codigo)) {
                lista.add(libro);
                break;
            }
            else if(
                    ( titulo != null && titulo.equals(libro.getTitulo()) ) ||
                            ( nombreAutor != null && esAutor(libro, nombreAutor) )
            ){
                lista.add(libro);
            }
        }
        return lista;
    }
    private Boolean esAutor(Libro libro, String nombreAutor){
        Boolean existe = false;
        for (Autor autor: libro.getAutores()) {
            if(autor.getNombres().equals(nombreAutor)){
                existe = true;
                break;
            }
        }
        return existe;
    }
}
