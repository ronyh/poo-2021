package pe.edu.uni.fiis.examen.parcial.pregunta3.dto;

public class Venta {
    private String producto;
    private Integer cantidad;
    private Double precioTotal;

    public int hashCode() {
        return this.producto.hashCode();
    }

    public boolean equals(Object obj) {
        Venta venta = (Venta) obj;
        return this.producto.equals(venta.producto);
    }

    public Venta(String producto, Integer cantidad, Double precioTotal) {
        this.producto = producto;
        this.cantidad = cantidad;
        this.precioTotal = precioTotal;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }
}
