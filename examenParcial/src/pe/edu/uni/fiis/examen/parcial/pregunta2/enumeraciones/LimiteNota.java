package pe.edu.uni.fiis.examen.parcial.pregunta2.enumeraciones;

public enum LimiteNota {
    SUPERIOR(20),INFERIOR(0);
    private Integer valor;

    LimiteNota(Integer valor) {
        this.valor = valor;
    }

    public Integer getValor() {
        return valor;
    }
}
