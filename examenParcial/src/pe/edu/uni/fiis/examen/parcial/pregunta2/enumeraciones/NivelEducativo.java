package pe.edu.uni.fiis.examen.parcial.pregunta2.enumeraciones;

public enum NivelEducativo {
    PRIMARIA("Primaria"), SECUNDRIA("Secundaria"), SUPERIOR("Superior");
    private String valor;

    NivelEducativo(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
