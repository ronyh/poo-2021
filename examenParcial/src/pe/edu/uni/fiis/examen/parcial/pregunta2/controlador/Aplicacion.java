package pe.edu.uni.fiis.examen.parcial.pregunta2.controlador;

import pe.edu.uni.fiis.examen.parcial.pregunta2.excepciones.NotaExcepcion;
import pe.edu.uni.fiis.examen.parcial.pregunta2.servicio.Servicio;
import pe.edu.uni.fiis.examen.parcial.pregunta2.servicio.ServicioImpl;
import pe.edu.uni.fiis.examen.parcial.pregunta2.dto.Postulante;
import pe.edu.uni.fiis.examen.parcial.pregunta2.enumeraciones.NivelEducativo;

import java.util.HashMap;
import java.util.Map;

public class Aplicacion {
    public static void main(String[] args) {
        Postulante primero = new Postulante(12012,"Ronald","Granda","Los faisanes 125","9986795",36, NivelEducativo.SUPERIOR);
        Postulante segundo = new Postulante(12013,"Milton","Magallanes","Los perales 125","9986569",40, NivelEducativo.SUPERIOR);
        Map<Integer, Postulante> mapa = new HashMap<>();
        Servicio servicio = new ServicioImpl();
        servicio.registrarPostulante(primero);
        servicio.registrarPostulante(segundo);
        try {
            servicio.asignarNota(primero.getNumeroIdentidad(),22);
        } catch (NotaExcepcion notaExcepcion) {
            notaExcepcion.printStackTrace();
        }
    }
}
