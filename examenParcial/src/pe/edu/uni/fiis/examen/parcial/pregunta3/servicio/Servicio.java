package pe.edu.uni.fiis.examen.parcial.pregunta3.servicio;

import pe.edu.uni.fiis.examen.parcial.pregunta3.dto.Venta;

public interface Servicio {
    void registrarVenta(Venta venta);
    void mostrarReporteRelojesVendidos();
    void mostrarRankingtop3();
}
