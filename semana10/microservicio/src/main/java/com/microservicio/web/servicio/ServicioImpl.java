package com.microservicio.web.servicio;

import com.microservicio.web.dao.Dao;
import com.microservicio.web.dto.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public List<Producto> obtenerProductos() {
        List<Producto> lista = new ArrayList<>();
        try {
            lista = dao.obtenerProductos();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }
}
