package com.microservicio.web.dao;

import com.microservicio.web.dto.Producto;

import java.sql.SQLException;
import java.util.List;

public interface Dao {
    public List<Producto> obtenerProductos() throws SQLException;
}
