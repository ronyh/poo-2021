package com.microservicio.web.dto.rest.producto;

import com.microservicio.web.dto.Producto;
import lombok.Data;

import java.util.List;
@Data
public class RespuestaProducto {
    private List<Producto> lista;
}
