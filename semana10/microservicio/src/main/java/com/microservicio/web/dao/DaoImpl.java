package com.microservicio.web.dao;

import com.microservicio.web.dto.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void obtenerConexion() throws SQLException {
        conexion = jdbcTemplate.getDataSource().getConnection();
    }
    private void cerrarConexion() throws SQLException{
        conexion.close();
        conexion = null;
    }
    public List<Producto> obtenerProductos() throws SQLException {
        List<Producto> productos = new ArrayList<>();
        obtenerConexion();
        Statement sentencia = conexion.createStatement();
        String sql =    " SELECT codigo, nombre "+
                " FROM producto ";
        ResultSet resultado = sentencia.executeQuery(sql);
        while(resultado.next()){
            Producto producto = new Producto(resultado.getInt("codigo"), resultado.getString("nombre"));
            productos.add(producto);
        }
        resultado.close();
        sentencia.close();
        cerrarConexion();
        return productos;
    }
}
