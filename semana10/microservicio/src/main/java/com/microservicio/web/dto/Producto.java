package com.microservicio.web.dto;

import lombok.Data;

@Data
public class Producto {
    private Integer codigo;
    private String nombre;

    public Producto(Integer codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }
}
