create table tipo_cuaderno(
                              id_tipo numeric(3) primary key ,
                              descripcion varchar(100)
);

create table cuaderno(
                         id_cuaderno numeric(9) primary key,
                         id_tipo numeric(3),
                         nombre varchar(100)
);
insert into tipo_cuaderno(id_tipo, descripcion) values (1,'Espiralado');
insert into tipo_cuaderno(id_tipo, descripcion) values (2,'Reciclado');

insert into cuaderno(id_cuaderno, id_tipo, nombre) values(1,1,'Alpha A4');
insert into cuaderno(id_cuaderno, id_tipo, nombre) values(2,2,'Beta A3');
insert into cuaderno(id_cuaderno, id_tipo, nombre) values(3,2,'Gamma A2');

select id_tipo, descripcion
from tipo_cuaderno;

select id_cuaderno, id_tipo, nombre
from cuaderno;

select c.id_cuaderno, tc.descripcion , c.nombre
from cuaderno c
         join tipo_cuaderno tc on c.id_tipo = tc.id_tipo;


select c.id_cuaderno, tc.descripcion , c.nombre
from cuaderno c
         join tipo_cuaderno tc on c.id_tipo = tc.id_tipo
where c.id_cuaderno = 1

create table usuario(
                        codigo_usuario varchar(40),
                        clave varchar(100)
);
alter table usuario add column nombre varchar(100);
insert into usuario(codigo_usuario, clave) VALUES ('ronyh','2021');
insert into usuario(codigo_usuario, clave) VALUES ('erik','2099');

update usuario
set nombre = 'Rony Hancco'
where codigo_usuario = 'ronyh';

update usuario
set nombre = 'El delegado'
where codigo_usuario = 'erik';

select usuario.codigo_usuario, usuario.clave, usuario.nombre
from usuario;


select usuario.codigo_usuario, usuario.clave, usuario.nombre
from usuario
where codigo_usuario = 'ronyh' and clave = '2021';