let nombre = 'Rony';
console.log('Nombre: ' + nombre);

nombre = 1;
console.log('Nombre: ' + nombre);

nombre = 1.1;
console.log('Nombre: ' + nombre);

nombre = 'Rony';
console.log('Nombre: ' + nombre);

nombre = {
    nombre: 'Rony',
    apellidos: 'Hancco',
    cargo: 'Docente'
};
nombre.nombre = 'Rony Jordan';
console.log('Nombre: ' + nombre.nombre);
console.log('Nombre: ' + nombre.apellidos);
console.log('Nombre: ' + nombre.cargo);

function mifuncion(a, b){
    console.log('mifuncion1');
    console.log(a);
    console.log(b);
    var nombre = document.querySelector('#nombre');
    var panel = document.querySelector('#panel');
    panel.innerHTML = '<p>' + a + " "  + nombre.value +' </p>';
    if(nombre.value == 'Rony'){
        nombre.style.color='#ff0000';
    }
    nombre.value = 'Programación';

}

var mifuncion2 = function(a,b){
    console.log('mifunction2');
    console.log(a);
    console.log(b);
    var juego = document.querySelector('#juego');
    juego.value = 'ti';
}

var mifuncion3 = (a,b) => {
    console.log('mifunction3');
    console.log(a);
    for (let i = 0; i < 5; i++) {
        console.log(b)
    }
    console.log(b);
    var titulo = document.querySelector('h1');
    titulo.className = 'gigante';

};
/*
mifuncion(2,4);
mifuncion2(5,7);
mifuncion3(9,11);
*/
let sumar = (primero, segundo) => {
    let arreglo = [52,56];
    let alumno = {
      nombre: 'Rony',apellidos: 'Hancco',
      cursos: [
          {codigo: 'BIC01', nombre: 'Introducción a la Computación'},
          {codigo: 'SI302', nombre: 'Programación objetos'},
      ]
    };
    let nombre = document.querySelector('#nombre');
    nombre.value = (primero + segundo + arreglo[0] + arreglo[1])
        + alumno.nombre + ' ' + alumno.apellidos;
    let apellidos = document.querySelector('#apellidos');
    apellidos.value = alumno.cursos[0].nombre + alumno.cursos[1].nombre
};

//var -- let      variable
//    -- const    constante