package com.servicio.duplicaservicio.dto;

import lombok.Data;

@Data
public class Usuario {
    private String codigo;
    private String clave;
    private String nombre;
}
