package com.servicio.duplicaservicio.servicio;

import com.servicio.duplicaservicio.dto.Cuaderno;
import com.servicio.duplicaservicio.dto.Usuario;

import java.util.List;

public interface Servicio {
    public List<Cuaderno> obtenerCuadernos();
    public Cuaderno obtenerCuaderno(Cuaderno cuaderno);
    public Usuario obtenerUsuario(Usuario usuario);
}
