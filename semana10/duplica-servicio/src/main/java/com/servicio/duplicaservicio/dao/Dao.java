package com.servicio.duplicaservicio.dao;

import com.servicio.duplicaservicio.dto.Cuaderno;
import com.servicio.duplicaservicio.dto.Usuario;

import java.util.List;

public interface Dao {
    public List<Cuaderno> obtenerCuadernos();
    public Cuaderno obtenerCuaderno(Cuaderno cuaderno);
    public Usuario obtenerUsuario(Usuario usuario);
}
