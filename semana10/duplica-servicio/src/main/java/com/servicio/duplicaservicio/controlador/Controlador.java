package com.servicio.duplicaservicio.controlador;

import com.servicio.duplicaservicio.dto.Cuaderno;
import com.servicio.duplicaservicio.dto.Usuario;
import com.servicio.duplicaservicio.dto.rest.cuaderno.RespuestaCuaderno;
import com.servicio.duplicaservicio.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controlador {
    @Autowired
    private Servicio servicio;
    @RequestMapping(value = "/obtener-cuadernos", method = RequestMethod.POST,
            produces = "application/json;charset=utf-8",consumes = "application/json;charset=utf-8")
    public @ResponseBody RespuestaCuaderno obtenerCuadernos(){
        RespuestaCuaderno respuestaCuaderno = new RespuestaCuaderno();
        respuestaCuaderno.setLista(servicio.obtenerCuadernos());
        return respuestaCuaderno;
    }
    @RequestMapping(value = "/obtener-cuaderno", method = RequestMethod.POST,
            produces = "application/json;charset=utf-8",consumes = "application/json;charset=utf-8")
    public @ResponseBody Cuaderno obtenerCuaderno(@RequestBody Cuaderno cuaderno){
        return servicio.obtenerCuaderno(cuaderno);
    }

    @RequestMapping(value = "/obtener-usuario", method = RequestMethod.POST,
            produces = "application/json;charset=utf-8",consumes = "application/json;charset=utf-8")
    public @ResponseBody Usuario obtenerUsuario(@RequestBody Usuario Usuario){
        return servicio.obtenerUsuario(Usuario);
    }
}
