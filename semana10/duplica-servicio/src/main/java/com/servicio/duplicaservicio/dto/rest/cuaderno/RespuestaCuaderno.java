package com.servicio.duplicaservicio.dto.rest.cuaderno;

import com.servicio.duplicaservicio.dto.Cuaderno;
import lombok.Data;

import java.util.List;
@Data
public class RespuestaCuaderno {
    private List<Cuaderno> lista;
}
