package com.servicio.duplicaservicio.servicio;

import com.servicio.duplicaservicio.dao.Dao;
import com.servicio.duplicaservicio.dto.Cuaderno;
import com.servicio.duplicaservicio.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public List<Cuaderno> obtenerCuadernos(){
        return dao.obtenerCuadernos();
    }
    public Cuaderno obtenerCuaderno(Cuaderno cuaderno){
        return dao.obtenerCuaderno(cuaderno);
    }

    public Usuario obtenerUsuario(Usuario usuario) {
        return dao.obtenerUsuario(usuario);
    }
}
