package com.servicio.duplicaservicio.dto;

import lombok.Data;

@Data
public class Cuaderno {
    private Integer id;
    private String tipo;
    private String nombre;
}
