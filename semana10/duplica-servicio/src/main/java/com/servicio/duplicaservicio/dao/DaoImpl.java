package com.servicio.duplicaservicio.dao;

import com.servicio.duplicaservicio.dto.Cuaderno;
import com.servicio.duplicaservicio.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void obtenerConexion() throws SQLException {
        conexion = jdbcTemplate.getDataSource().getConnection();
    }
    private void cerrarConexion() throws SQLException {
        conexion.commit();
        conexion.close();
        conexion = null;
    }
    public List<Cuaderno> obtenerCuadernos(){
        List<Cuaderno> lista = new ArrayList<>();
        try {
            obtenerConexion();
            Statement sentencia = conexion.createStatement();
            String SQL = " select c.id_cuaderno, tc.descripcion , c.nombre\n" +
                    " from cuaderno c\n" +
                    " join tipo_cuaderno tc on c.id_tipo = tc.id_tipo";
            ResultSet resultado = sentencia.executeQuery(SQL);
            while(resultado.next()){
                Cuaderno cuaderno = new Cuaderno();
                cuaderno.setId(resultado.getInt("id_cuaderno"));
                cuaderno.setTipo(resultado.getString("descripcion"));
                cuaderno.setNombre(resultado.getString("nombre"));
                lista.add(cuaderno);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }
    public Cuaderno obtenerCuaderno(Cuaderno cuaderno){
        Cuaderno cuadernoRetorno = new Cuaderno();
        try {
            obtenerConexion();
            String SQL = " select c.id_cuaderno, tc.descripcion , c.nombre\n" +
                    " from cuaderno c\n" +
                    " join tipo_cuaderno tc on c.id_tipo = tc.id_tipo\n" +
                    " where c.id_cuaderno = ? ";
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setInt(1, cuaderno.getId());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                cuadernoRetorno.setId(resultado.getInt("id_cuaderno"));
                cuadernoRetorno.setTipo(resultado.getString("descripcion"));
                cuadernoRetorno.setNombre(resultado.getString("nombre"));
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return cuadernoRetorno;
    }

    public Usuario obtenerUsuario(Usuario usuario) {
        Usuario respuesta = new Usuario();
        try {
            obtenerConexion();
            String SQL = " select usuario.codigo_usuario, usuario.clave, usuario.nombre\n" +
                    " from usuario\n" +
                    " where codigo_usuario = ? and clave = ?";
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,usuario.getCodigo());
            sentencia.setString(2,usuario.getClave());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                respuesta.setCodigo(resultado.getString("codigo_usuario"));
                respuesta.setClave(resultado.getString("clave"));
                respuesta.setNombre(resultado.getString("nombre"));
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return respuesta;
    }
    public Usuario actualizarUsuario(Usuario usuario) {
        try {
            obtenerConexion();
            String SQL = " update usuario " +
                    " set clave = ? " +
                    " where codigo_usuario = ? ";
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,usuario.getClave());
            sentencia.setString(2,usuario.getCodigo());
            int filas = sentencia.executeUpdate();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }
    public Usuario agregarUsuario(Usuario usuario) {
        try {
            obtenerConexion();
            String SQL = " insert into usuario(codigo_usuario, clave) " +
                    " values(?,?)  ";
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,usuario.getCodigo());
            sentencia.setString(2,usuario.getClave());
            int filas = sentencia.executeUpdate();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }
}
