package com.servicio.duplicaservicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DuplicaServicioApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuplicaServicioApplication.class, args);
    }

}
