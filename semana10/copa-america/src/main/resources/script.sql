create table seleccion(
                          id_seleccion numeric(2,0),
                          nombre_federacion varchar(100),
                          nombre_pais varchar(30),
                          puntaje numeric(2,0)
);


create table jugador(
                        id_seleccion numeric(2,0),
                        numero numeric(2,0),
                        nombre varchar(100)
);

create table partido(
                        id_partido numeric(2,0),
                        id_seleccion_local numeric(2,0),
                        id_seleccion_visitante numeric(2,0),
                        fecha date,
                        hora time,
                        goles_local numeric(2,0),
                        goles_visitante numeric(2,0)
);

insert into seleccion(id_seleccion, nombre_federacion, nombre_pais, puntaje)
VALUES (1,'Federación peruana de Fútbol','Perú',3);
insert into seleccion(id_seleccion, nombre_federacion, nombre_pais, puntaje)
VALUES (2,'Federación colombiana de Fútbol','Colombia',3);

insert into jugador(id_seleccion, numero, nombre) VALUES (1,9,'Paolo  Guerrero');
insert into jugador(id_seleccion, numero, nombre) VALUES (1,1,'Gallece');

insert into partido(id_partido, id_seleccion_local, id_seleccion_visitante, fecha, hora, goles_local, goles_visitante)
VALUES(2,1,2,str_to_date('20210620','%Y%m%d'), null,3,1);

select * from partido;


-- Mostrar los nombres y números de los jugadores de una selección.

select nombre, numero
from jugador
where id_seleccion = 1;

-- Mostrar los nombres de las selecciones, fecha y goles de todos los partidos.

select sl.nombre_pais local, sv.nombre_pais visita, p.fecha, p.goles_local, p.goles_visitante
from partido p
join seleccion sl on (p.id_seleccion_local = sl.id_seleccion)
join seleccion sv on (p.id_seleccion_visitante = sv.id_seleccion);

-- Agregar una nueva selección.

insert into seleccion(id_seleccion, nombre_federacion, nombre_pais, puntaje)
VALUES (3,'Federación argentina de Fútbol','Argentina',0);

-- Agregar un nuevo jugador.
insert into jugador(id_seleccion, numero, nombre) VALUES (3,10,'Messi');

-- Agregar un nuevo partido.
insert into partido(id_partido, id_seleccion_local, id_seleccion_visitante, fecha, hora, goles_local, goles_visitante)
VALUES(3,1,3,str_to_date('20210624','%Y%m%d'), null,3,2);