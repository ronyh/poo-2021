package com.copa.america.copaamerica.servicio;

import com.copa.america.copaamerica.dao.Dao;
import com.copa.america.copaamerica.dto.Jugador;
import com.copa.america.copaamerica.dto.Partido;
import com.copa.america.copaamerica.dto.Seleccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public List<Jugador> obtenerJugadores(Seleccion seleccion) {
        return dao.obtenerJugadores(seleccion);
    }

    public List<Partido> obtenerPartidos() {
        return dao.obtenerPartidos();
    }

    public Seleccion agregarSeleccion(Seleccion seleccion) {
        return null;
    }

    public Jugador agregarJugador(Jugador jugador) {
        return null;
    }

    public Partido agregarPartido(Partido partido) {
        return null;
    }
}
