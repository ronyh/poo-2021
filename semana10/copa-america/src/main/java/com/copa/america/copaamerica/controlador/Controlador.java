package com.copa.america.copaamerica.controlador;

import com.copa.america.copaamerica.dto.RespuestaJugador;
import com.copa.america.copaamerica.dto.RespuestaPartido;
import com.copa.america.copaamerica.dto.Seleccion;
import com.copa.america.copaamerica.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controlador {
    @Autowired
    private Servicio servicio;

    @RequestMapping(value = "/obtener-jugadores", method = RequestMethod.POST,
            consumes = "application/json;charset:utf-8",
            produces = "application/json;charset:utf-8"
    )
    public @ResponseBody RespuestaJugador obtenerJugador(@RequestBody Seleccion seleccion){
        RespuestaJugador respuestaJugador = new RespuestaJugador();
        respuestaJugador.setLista(servicio.obtenerJugadores(seleccion));
        return respuestaJugador;
    }

    @RequestMapping(value = "/obtener-partidos", method = RequestMethod.POST,
            produces = "application/json;charset:utf-8"
    )
    public @ResponseBody RespuestaPartido obtenerPartidos(){
        RespuestaPartido respuestaPartido = new RespuestaPartido();
        respuestaPartido.setLista(servicio.obtenerPartidos());
        return respuestaPartido;
    }
}
