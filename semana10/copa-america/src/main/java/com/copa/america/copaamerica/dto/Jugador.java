package com.copa.america.copaamerica.dto;

import lombok.Data;

@Data
public class Jugador {
    private Integer id;
    private Integer numero;
    private String nombre;
}
