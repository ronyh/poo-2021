package com.copa.america.copaamerica.dto;

import lombok.Data;

import java.util.List;

@Data
public class RespuestaPartido {
    private List<Partido> lista;
}
