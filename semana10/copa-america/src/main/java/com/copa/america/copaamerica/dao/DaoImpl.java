package com.copa.america.copaamerica.dao;

import com.copa.america.copaamerica.dto.Jugador;
import com.copa.america.copaamerica.dto.Partido;
import com.copa.america.copaamerica.dto.Seleccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void crearConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    public List<Jugador> obtenerJugadores(Seleccion seleccion) {
        List<Jugador> lista = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select nombre, numero").
            append(" from jugador").
            append(" where id_seleccion = ?");
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sb.toString());
            sentencia.setInt(1, seleccion.getId());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                Jugador jugador = new Jugador();
                jugador.setNombre(resultado.getString("nombre"));
                jugador.setNumero(resultado.getInt("numero"));
                lista.add(jugador);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public List<Partido> obtenerPartidos() {
        List<Partido> lista = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select sl.nombre_pais local, sv.nombre_pais visita, DATE_FORMAT(p.fecha,'%d/%m/%Y') fecha, p.goles_local, p.goles_visitante\n").
                append(" from partido p\n").
                append(" join seleccion sl on (p.id_seleccion_local = sl.id_seleccion)\n").
                append(" join seleccion sv on (p.id_seleccion_visitante = sv.id_seleccion)");
        crearConexion();
        try {
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sb.toString());
            while(resultado.next()){
                Partido partido = new Partido();
                partido.setLocal(new Seleccion());
                partido.getLocal().setPais(resultado.getString("local"));
                partido.setVisitante(new Seleccion());
                partido.getVisitante().setPais(resultado.getString("visita"));
                partido.setFecha(resultado.getString("fecha"));
                partido.setGolesLocal(resultado.getInt("goles_local"));
                partido.setGolesVisita(resultado.getInt("goles_visitante"));
                lista.add(partido);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public Seleccion agregarSeleccion(Seleccion seleccion) {
        return null;
    }

    public Jugador agregarJugador(Jugador jugador) {
        return null;
    }

    public Partido agregarPartido(Partido partido) {
        return null;
    }
}
