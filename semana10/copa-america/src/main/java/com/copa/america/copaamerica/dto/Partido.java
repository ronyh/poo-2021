package com.copa.america.copaamerica.dto;

import lombok.Data;

@Data
public class Partido {
    private Integer id;
    private Seleccion local;
    private Seleccion visitante;
    private String fecha;
    private String hora;
    private Integer golesLocal;
    private Integer golesVisita;
}
