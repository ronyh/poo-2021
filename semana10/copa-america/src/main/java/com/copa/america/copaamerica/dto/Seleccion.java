package com.copa.america.copaamerica.dto;

import lombok.Data;

@Data
public class Seleccion {
    private Integer id;
    private String federacion;
    private String pais;
    private Integer puntaje;
}
