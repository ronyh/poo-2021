package com.copa.america.copaamerica.dao;

import com.copa.america.copaamerica.dto.Jugador;
import com.copa.america.copaamerica.dto.Partido;
import com.copa.america.copaamerica.dto.Seleccion;

import java.util.List;

public interface Dao {
    public List<Jugador> obtenerJugadores(Seleccion seleccion);
    public List<Partido> obtenerPartidos();
    public Seleccion agregarSeleccion(Seleccion seleccion);
    public Jugador agregarJugador(Jugador jugador);
    public Partido agregarPartido(Partido partido);
}
