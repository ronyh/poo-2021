package com.rony.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class Producto {
    /**
     * `id_producto` DECIMAL(9,0) NOT NULL,
     * 	`id_marca` DECIMAL(9,0) NOT NULL,
     * 	`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
     * 	`peso` DECIMAL(5,2) NOT NULL,
     * 	`precio_regular` DECIMAL(7,2) NOT NULL,
     * 	`precio_oferta` DECIMAL(7,2) NOT NULL,
     * 	`oferta` CHAR(1) NOT NULL COLLATE 'utf8_general_ci',
     * 	`calificacion` DECIMAL(1,0) NOT NULL,
     * 	`url_imagen` VARCHAR(200) NOT NULL COLLATE 'utf8_general_ci',
     */
    private Integer idProducto;
    private Marca marca;
    private String nombre;
    private Double peso;
    private Double precioRegular;
    private Double precioOferta;
    private Boolean oferta;
    private Integer calificacion;
    private String urlImagen;

    public Producto(Integer idProducto, Marca marca, String nombre, Double peso, Double precioRegular, Double precioOferta, Boolean oferta, Integer calificacion, String urlImagen) {
        this.idProducto = idProducto;
        this.marca = marca;
        this.nombre = nombre;
        this.peso = peso;
        this.precioRegular = precioRegular;
        this.precioOferta = precioOferta;
        this.oferta = oferta;
        this.calificacion = calificacion;
        this.urlImagen = urlImagen;
    }
}
