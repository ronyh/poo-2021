package com.rony.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class Usuario {
    /**
     * CREATE TABLE usuario(
     * 	id_usuario NUMERIC(9) PRIMARY KEY,
     * 	nombres VARCHAR(100),
     * 	apellidos VARCHAR(100),
     * 	correo VARCHAR(200) UNIQUE,
     * 	administrador CHAR(1),
     * 	clave VARCHAR(200)
     * );
     */
    private Integer idUsuario;
    private String nombres;
    private String apellidos;
    private String correo;
    private String administrador;
    private String clave;

    public Usuario(Integer idUsuario, String nombres, String apellidos, String correo, String administrador, String clave) {
        this.idUsuario = idUsuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.administrador = administrador;
        this.clave = clave;
    }

    public Usuario() {
    }
}
