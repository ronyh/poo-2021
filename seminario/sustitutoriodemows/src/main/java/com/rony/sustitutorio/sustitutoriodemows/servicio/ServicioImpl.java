package com.rony.sustitutorio.sustitutoriodemows.servicio;

import com.rony.sustitutorio.sustitutoriodemows.dao.Dao;
import com.rony.sustitutorio.sustitutoriodemows.dto.model.Producto;
import com.rony.sustitutorio.sustitutoriodemows.dto.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public Usuario autenticarUsuario(Usuario usuario) {
        return dao.autenticarUsuario(usuario);
    }

    public Usuario agregarUsuario(Usuario usuario) {
        if(dao.validarExistenciaCorreo(usuario.getCorreo())){
            return new Usuario();
        }
        else{
            return dao.agregarUsuario(usuario);
        }
    }
    public List<Producto> buscarProductos(Producto producto){
        return dao.buscarProductos(producto);
    }
}
