package com.rony.sustitutorio.sustitutoriodemows.controlador;

import com.rony.sustitutorio.sustitutoriodemows.dto.model.Producto;
import com.rony.sustitutorio.sustitutoriodemows.dto.model.Usuario;
import com.rony.sustitutorio.sustitutoriodemows.dto.rest.RespuestaProducto;
import com.rony.sustitutorio.sustitutoriodemows.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;
    @RequestMapping(
            value = "autenticar-usuario",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario autenticarUsuario(@RequestBody Usuario usuario){
        if(usuario == null || usuario.getIdUsuario() != null
                || usuario.getNombres() != null
                || usuario.getApellidos() != null
                || usuario.getAdministrador() != null
        ){
            return new Usuario();
        }
        else{
            return servicio.autenticarUsuario(usuario);
        }
    }

    @RequestMapping(
            value = "agregar-usuario",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario agregarUsuario(@RequestBody Usuario usuario){
        return servicio.agregarUsuario(usuario);
    }
    @RequestMapping(
            value = "buscar-productos",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaProducto buscarProductos(@RequestBody Producto producto){
        RespuestaProducto respuestaProducto = new RespuestaProducto();
        respuestaProducto.setLista(servicio.buscarProductos(producto));
        return respuestaProducto;
    }
}
