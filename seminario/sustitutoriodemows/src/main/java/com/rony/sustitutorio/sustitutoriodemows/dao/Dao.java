package com.rony.sustitutorio.sustitutoriodemows.dao;

import com.rony.sustitutorio.sustitutoriodemows.dto.model.Producto;
import com.rony.sustitutorio.sustitutoriodemows.dto.model.Usuario;

import java.util.List;

public interface Dao {
    public Usuario autenticarUsuario(Usuario usuario);
    public Boolean validarExistenciaCorreo(String correo);
    public Usuario agregarUsuario(Usuario usuario);
    public List<Producto> buscarProductos(Producto producto);
}
