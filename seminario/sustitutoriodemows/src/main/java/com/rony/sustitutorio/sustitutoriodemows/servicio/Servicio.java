package com.rony.sustitutorio.sustitutoriodemows.servicio;

import com.rony.sustitutorio.sustitutoriodemows.dto.model.Producto;
import com.rony.sustitutorio.sustitutoriodemows.dto.model.Usuario;

import java.util.List;

public interface Servicio {
    public Usuario autenticarUsuario(Usuario usuario);
    public Usuario agregarUsuario(Usuario usuario);
    public List<Producto> buscarProductos(Producto producto);
}
