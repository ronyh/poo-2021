package com.rony.sustitutorio.sustitutoriodemows.dao;

import com.rony.sustitutorio.sustitutoriodemows.dto.model.Marca;
import com.rony.sustitutorio.sustitutoriodemows.dto.model.Producto;
import com.rony.sustitutorio.sustitutoriodemows.dto.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public Usuario autenticarUsuario(Usuario usuario) {
        String SQL = " SELECT id_usuario,nombres,apellidos,correo,administrador\n" +
                " FROM usuario\n" +
                " WHERE correo = ? AND clave = ? ";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1,usuario.getCorreo());
            sentencia.setString(2,usuario.getClave());
            ResultSet resultado = sentencia.executeQuery();
            usuario = new Usuario();
            while(resultado.next()){
                usuario = extraerUsuario(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }
    private Usuario extraerUsuario(ResultSet resultado) throws SQLException {
        Usuario retorno = new Usuario(
            resultado.getInt("id_usuario"),
            resultado.getString("nombres"),
            resultado.getString("apellidos"),
            resultado.getString("correo"),
            resultado.getString("administrador"),
                null
        );
        return retorno;
    }

    public Boolean validarExistenciaCorreo(String correo) {
        Boolean encontrado = false;
        String SQL = " SELECT id_usuario,nombres,apellidos,correo,administrador\n" +
                " FROM usuario\n" +
                " WHERE correo = ? ";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1,correo);
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                encontrado = true;
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return encontrado;
    }

    public Usuario agregarUsuario(Usuario usuario) {
        String SQL = " INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave)" +
                " VALUES(nextval(secuencia_usuario),?,?,?,?,?)";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1,usuario.getNombres());
            sentencia.setString(2,usuario.getApellidos());
            sentencia.setString(3,usuario.getCorreo());
            sentencia.setString(4,usuario.getAdministrador());
            sentencia.setString(5,usuario.getClave());
            sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }

    public List<Producto> buscarProductos(Producto producto) {
        List<Producto> lista = new ArrayList<>();
        boolean primero = true;
        Integer indice = 1;
        String SQL = " SELECT p.id_producto,\n" +
                "    p.id_marca,\n" +
                "    m.nombre nombre_marca,\n" +
                "    p.nombre nombre_producto,\n" +
                "    p.peso,\n" +
                "    p.precio_regular,\n" +
                "    p.precio_oferta,\n" +
                "    p.oferta,\n" +
                "    p.calificacion,\n" +
                "    p.url_imagen\n" +
                " FROM producto p\n" +
                " JOIN marca m\n" +
                " ON (p.id_marca = m.id_marca) \n";
        /*
                " WHERE upper(p.nombre) LIKE ?\n" +
                " OR UPPER(m.nombre) LIKE ? \n" +
                " OR p.precio_regular BETWEEN ? AND ?";
        */
        if(producto.getNombre() != null){
            SQL += " WHERE upper(p.nombre) LIKE ?\n";
            primero = false;
        }
        if(producto.getMarca().getNombre() != null){
            if(primero){
                SQL += " WHERE UPPER(m.nombre) LIKE ? \n";
                primero = false;
            }
            else{
                SQL += " OR UPPER(m.nombre) LIKE ? \n";
            }
        }
        if(producto.getPrecioOferta() != null && producto.getPrecioRegular() != null) {
            if(primero){
                SQL += " WHERE p.precio_regular BETWEEN ? AND ?";
            }
            else{
                SQL += " OR p.precio_regular BETWEEN ? AND ?";
            }
        }
        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            if(producto.getNombre() != null){
                sentencia.setString(indice,"%"+producto.getNombre()+"%");
                indice++;
            }
            if(producto.getMarca().getNombre() != null){
                sentencia.setString(indice,"%"+producto.getMarca().getNombre()+"%");
                indice++;
            }
            if(producto.getPrecioOferta() != null){
                sentencia.setDouble(indice,producto.getPrecioOferta());
                indice++;
            }
            if(producto.getPrecioRegular() != null){
                sentencia.setDouble(indice,producto.getPrecioRegular());
            }
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                Producto productoTemporal =  extraerProducto(resultado);
                lista.add(productoTemporal);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    private Producto extraerProducto(ResultSet resultado) throws SQLException {
        Producto retorno = new Producto(
                resultado.getInt("id_producto"),
                new Marca(resultado.getInt("id_marca"),resultado.getString("nombre_marca")),
                resultado.getString("nombre_producto"),
                resultado.getDouble("peso"),
                resultado.getDouble("precio_regular"),
                resultado.getDouble("precio_oferta"),
                resultado.getString("oferta").equals("1")? true: false,
                resultado.getInt("calificacion"),
                resultado.getString("url_imagen")
        );
        return retorno;
    }
}
