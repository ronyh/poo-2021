package com.rony.sustitutorio.sustitutoriodemows.dto.rest;

import com.rony.sustitutorio.sustitutoriodemows.dto.model.Producto;
import lombok.Data;

import java.util.List;

@Data
public class RespuestaProducto {
    private List<Producto> lista;
}
