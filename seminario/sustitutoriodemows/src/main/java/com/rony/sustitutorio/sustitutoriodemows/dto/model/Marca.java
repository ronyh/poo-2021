package com.rony.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class Marca {
        private Integer idMarca;
        private String nombre;

    public Marca(Integer idMarca, String nombre) {
        this.idMarca = idMarca;
        this.nombre = nombre;
    }
}
