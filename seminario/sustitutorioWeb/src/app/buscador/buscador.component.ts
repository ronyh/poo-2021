import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Producto } from '../interfaces';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.scss']
})
export class BuscadorComponent implements OnInit {
  nombreProducto: string = '';
  nombreMarca: string = '';
  precioMinimo: number = 0;
  precioMaximo: number = 0;
  productos: Producto[] = [];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
  }
  buscarProductos():void{
    const producto:Producto ={
      idProducto: null,
      marca: {
        idMarca: null,
        nombre: this.nombreMarca.trim() == '' ? null : this.nombreMarca.trim(),
      },
      nombre: this.nombreProducto.trim() == '' ? null : this.nombreProducto.trim(),
      peso: null,
      precioRegular: this.precioMaximo,
      precioOferta: this.precioMinimo,
      oferta: null,
      calificacion: null,
      urlImagen: null
    };
    console.log("buscarProductos");
    this.api.obtenerProductos(producto).subscribe( respuesta => {
      this.productos = respuesta.lista;
    });
  }

}
