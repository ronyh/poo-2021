package pe.edu.uni.fiis.laboratorio3.polinomio;

public class DerivadaConcreta extends DerivadaAbstract{
    public DerivadaConcreta(double[] polimonio) {
        super(polimonio);
    }

    public DerivadaAbstract calcularDerivada() {
        double[] derivada = new double[this.getPolimonio().length-1];
        for (int i = 0; i < derivada.length; i++) {
            derivada[i] = this.getPolimonio()[i]*(this.getPolimonio().length-1-i);
        }
        DerivadaAbstract a = new DerivadaConcreta(derivada);
        return a;
    }

    public static void main(String[] args) {
        double[] derivada = {4,25,2,1};
        DerivadaConcreta a = new DerivadaConcreta(derivada);
        a.escribirPolinomio();
        DerivadaAbstract z = a.calcularDerivada();
        z.escribirPolinomio();
    }
}
