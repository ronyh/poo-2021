package pe.edu.uni.fiis.laboratorio3.util;

import pe.edu.uni.fiis.laboratorio3.constructores.Reloj;
import pe.edu.uni.fiis.laboratorio3.constructores.RelojAutomatico;
import pe.edu.uni.fiis.laboratorio3.constructores.RelojBuilder;

public class Aplicacion {
    public static void main(String[] args) {
        Reloj r = RelojBuilder.getInstanceReloj(56,2,1,"Rojo");
        //r.color = "Azul";
        System.out.println(r.getHora());
        int aHora = 100;
        r.setHora(aHora);
        //RelojBuilder rb = new RelojBuilder();

        RelojAutomatico ra = new RelojAutomatico();
        ra.setHora(122);
        System.out.println(ra.getHora());
    }
}
