package pe.edu.uni.fiis.laboratorio3.constructores;
public class Reloj {
    private Integer hora;
    public Integer minuto;
    public Integer segundo;
    public String color;
    public Double energia = 0d;
    private String tipo;
    public void reducirEnergia(){
        if(this.tipo.equals("M")){
            this.energia = this.energia - 1;
        }
        else{
            this.energia = this.energia - 0.5 ;
        }
    }
    private static int hora2;
    static void imp(){
        hora2 = 2;
        Reloj a = new Reloj(1,2,1,"Rojo");
        a.segundo= 0;

    }

    public Reloj() {
        int y = 0;
        long t = y;
        float h = 0.0f;
        double j = 0.2d;
        System.out.println("Constructor Reloj");
    }

    Reloj(int hora, int minuto, int segundo, String color) {
        setHora(hora);
        this.minuto = minuto;
        this.segundo = segundo;
        this.color = color;
    }

    public void setHora(int hora) {
        if(hora > -1 && hora < 24){
            this.hora = hora;
        }
        else{
            this.hora = 0;
        }
    }

    public int getHora() {
        return hora;
    }

    public void setHora(Integer hora) {
        this.hora = hora;
    }

    public Integer getMinuto() {
        return minuto;
    }

    public void setMinuto(Integer minuto) {
        this.minuto = minuto;
    }

    public Integer getSegundo() {
        return segundo;
    }

    public void setSegundo(Integer segundo) {
        this.segundo = segundo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getEnergia() {
        return energia;
    }

    public void setEnergia(Double energia) {
        this.energia = energia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public static int getHora2() {
        return hora2;
    }

    public static void setHora2(int hora2) {
        Reloj.hora2 = hora2;
    }
}
