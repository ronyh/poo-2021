package pe.edu.uni.fiis.laboratorio3.polinomio;

public abstract class DerivadaAbstract {
    private double[] polimonio;

    public DerivadaAbstract(double[] polimonio) {
        this.polimonio = polimonio;
    }
    public abstract DerivadaAbstract calcularDerivada();

    public void escribirPolinomio(){
        System.out.println("Polinomio: ");
        for (int i = 0; i < polimonio.length; i++) {
            if(i != 0){
                System.out.print(" + ");
            }
            System.out.print(polimonio[i]+"X^"+(polimonio.length-1-i));
        }
        System.out.println("");
    }
    public double[] getPolimonio() {
        return polimonio;
    }

    public void setPolimonio(double[] polimonio) {
        this.polimonio = polimonio;
    }
}
