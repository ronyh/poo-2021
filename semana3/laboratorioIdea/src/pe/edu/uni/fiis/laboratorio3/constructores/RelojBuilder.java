package pe.edu.uni.fiis.laboratorio3.constructores;

public abstract class RelojBuilder {
    public static Reloj getInstanceReloj(int hora, int minuto, int segundo, String color){
        return new Reloj(hora, minuto, segundo, color);
    }
    public abstract void imprimir(int hora);

    public RelojBuilder() {
    }
}
