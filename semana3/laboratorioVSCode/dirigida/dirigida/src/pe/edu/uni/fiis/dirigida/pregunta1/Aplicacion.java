package pe.edu.uni.fiis.dirigida.pregunta1;

public class Aplicacion {
    public static void main(String[] args) {
        Producto laRepublica = new Producto();
        laRepublica.setNombre("Diario La República"); 
        laRepublica.setPrecioUnitario(1.5); 
        laRepublica.setTipo("D");

        Producto gestion = new Producto();
        gestion.setNombre("Diario Gestión"); 
        gestion.setPrecioUnitario(4); 
        gestion.setTipo("D");

        Producto caretas = new Producto();
        caretas.setNombre("Revista Caretas"); 
        caretas.setPrecioUnitario(5); 
        caretas.setTipo("R");

        Producto newFiis = new Producto();
        newFiis.setNombre("Revista News Fiis"); 
        newFiis.setPrecioUnitario(10); 
        newFiis.setTipo("R");

        Plan planZ = new Plan();
        Producto[] productosZ = {laRepublica,newFiis};
        planZ.setProductos(productosZ);
        planZ.setPrecio(10);

        Plan planR = new Plan();
        Producto[] productosR = {gestion,caretas};
        planR.setProductos(productosR);
        planR.setPrecio(8);

        Cliente rony = new Cliente("40404040", "Rony Hancco");

        Suscripcion suscripcion = new Suscripcion();
        suscripcion.setCliente(rony);
        suscripcion.setPlan(planR);

        Cliente renzo = new Cliente("40404000", "Renzo Tapia");

        Suscripcion suscripcionT = new Suscripcion();
        suscripcionT.setCliente(renzo);
        suscripcionT.setPlan(planZ);

        Suscripcion[] lista = {suscripcion, suscripcionT};

        ConsolidadoVenta venta = new ConsolidadoVenta();
        venta.setAnio(2021);
        venta.setMes(4);
        venta.setMonto(Suscripcion.calcularMonto(lista));;
        System.out.println(venta.getMonto());

    }
}
