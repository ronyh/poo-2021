package pe.edu.uni.fiis.dirigida.pregunta2;

public class Curso {
    private String nombre;
    private int creditos;
    private Nota[] notas;
    public String sistemaEvaluacion;

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getCreditos() {
        return creditos;
    }
    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }
    public Nota[] getNotas() {
        return notas;
    }
    public void setNotas(Nota[] notas) {
        this.notas = notas;
    }
    public String getSistemaEvaluacion() {
        return sistemaEvaluacion;
    }
    public void setSistemaEvaluacion(String sistemaEvaluacion) {
        this.sistemaEvaluacion = sistemaEvaluacion;
    }
    

}
