package pe.edu.uni.fiis.dirigida.pregunta2;

public class Nota {
    private String tipo;
    private int valor;
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public int getValor() {
        return valor;
    }
    public void setValor(int valor) {
        this.valor = valor;
    }
    
}
