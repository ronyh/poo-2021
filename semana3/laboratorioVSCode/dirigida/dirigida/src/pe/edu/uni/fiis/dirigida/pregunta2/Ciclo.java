package pe.edu.uni.fiis.dirigida.pregunta2;

public class Ciclo {
    private Curso[] cursos;
    private double promedio;
    public Curso[] getCursos() {
        return cursos;
    }
    public void setCursos(Curso[] cursos) {
        this.cursos = cursos;
    }
    public double getPromedio() {
        return promedio;
    }
    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
    
}
