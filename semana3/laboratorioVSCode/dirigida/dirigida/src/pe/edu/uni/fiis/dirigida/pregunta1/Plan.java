package pe.edu.uni.fiis.dirigida.pregunta1;

public class Plan {
    private double precio;
    private Producto[] productos;
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public Producto[] getProductos() {
        return productos;
    }
    public void setProductos(Producto[] productos) {
        this.productos = productos;
    }
    
}
