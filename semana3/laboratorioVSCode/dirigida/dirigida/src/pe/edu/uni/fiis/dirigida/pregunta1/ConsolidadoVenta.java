package pe.edu.uni.fiis.dirigida.pregunta1;

public class ConsolidadoVenta {
    private double monto;
    private int anio;
    private int mes;
    
    public double getMonto() {
        return monto;
    }
    public void setMonto(double monto) {
        this.monto = monto;
    }
    public int getAnio() {
        return anio;
    }
    public void setAnio(int anio) {
        this.anio = anio;
    }
    public int getMes() {
        return mes;
    }
    public void setMes(int mes) {
        this.mes = mes;
    }
    
}
