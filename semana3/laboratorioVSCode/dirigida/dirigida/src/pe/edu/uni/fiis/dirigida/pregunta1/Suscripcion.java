package pe.edu.uni.fiis.dirigida.pregunta1;

import java.util.Date;

public class Suscripcion {
    private Plan plan;
    private Cliente cliente;
    private Date fechaInicio;
    private Date fechaFin;
    public static double calcularMonto(Suscripcion[] suscripciones){
        double monto = 0;
        for (Suscripcion suscripcion : suscripciones) {
            monto += suscripcion.getPlan().getPrecio();
        }
        return monto;
    }
    public Plan getPlan() {
        return plan;
    }
    public void setPlan(Plan plan) {
        this.plan = plan;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public Date getFechaInicio() {
        return fechaInicio;
    }
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    public Date getFechaFin() {
        return fechaFin;
    }
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
    
}
